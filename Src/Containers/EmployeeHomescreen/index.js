import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, FlatList, Image, ScrollView, SafeAreaView, TouchableOpacity } from 'react-native';
import HeaderComponent from "../../Components/Header"
import FontSize from '../../Utils/fonts';
import ButtonComponent from '../../Components/Button/loginButton'
//import {  } from 'react-native-gesture-handler';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Colors from "./../../Utils/Colors";
import FontFamily from '../../Utils/FontFamily';
import AsyncStorage from '@react-native-community/async-storage';
const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;


const DATA = [
  {
    id: 1,
    name: "Stuart Alexandra",
    desingnation: "Project Manager",
    companyname: "Brainium Information Technology",
    updated: "Last updated 2 days ago.",
    profilePercentage: 89,
    image: require('../../Images/EmployerProfile/image.jpg')
  },
];


class EmployeeHomescreen extends Component {
  state = {
    postjob: '',
    storage: ''
  }

  async componentDidMount() {
    const storage = await AsyncStorage.getItem('usertype');
    this.setState({
      storage: storage
    })
    console.log(this.state.storage, "storage");
  }


  render() {
    return (
      //Main Container
      <ScrollView>
      <View style={styles.MainContainer}>
          {this.state.storage == "employees" ?
            <HeaderComponent
              drawericon
              message
              dot
              onClick={() => this.props.navigation.openDrawer()}
              data={DATA}
              headerProfile={"Header Profile"}
              type={"textinput"}
              onChangeText={(postjob) => this.setState({ postjob })}
              placeholder="Search Job"
              messageiconpress={() => this.props.navigation.navigate('MessageScreen')}
              editprofile={() => this.props.navigation.navigate('EditProfile')}
            />
            :
            <HeaderComponent
              drawericon
              message
              dot
              onClick={() => this.props.navigation.openDrawer()}
              data={DATA}
              headerProfile={"Header Profile"}
              messageiconpress={() => this.props.navigation.navigate('MessageScreen')}
              editprofile={() => this.props.navigation.navigate('EditProfile')}
            />
          }

          <View style={styles.firstBodycontainer}>
                <View style={styles.performenceContainer}>
                  <Text style={styles.performance}>Your profile perfomance</Text>
                  <View style={styles.apperancesContainer}>
                    <View style={{}}>
                      <Text style={styles.apperancesNumber}>1236</Text>
                      <Text style={styles.apperances}>Search Apperances</Text>
                    </View>
                    <View>
                      <Text style={styles.recuiterActionNumber}>12</Text>
                      <Text style={styles.recuiterAction}>Recruiter Actions</Text>
                    </View>
                  </View>
                  {
                  this.state.storage == "employers" ?
                  <TouchableOpacity style={{ marginBottom: 0, flexDirection: "row" }}>
                    <Text style={styles.missingProfileText} >1 PROFILE DETAIL IS MISSING</Text>
                    <AntDesign
                      name="right"
                      size={13}
                      color="#384476"
                      style={{ marginTop: 4, marginLeft: 5 }}
                    />
                  </TouchableOpacity>
                  :
                  <TouchableOpacity style={{ marginBottom: 0, flexDirection: "row" }} onPress={() => this.props.navigation.navigate('MainFrom', {step: 1})}>
                    <Text style={styles.missingProfileText}>PLEASE FILL UP ALL PROFILE DETAILS</Text>
                    <AntDesign
                      name="right"
                      size={13}
                      color="#384476"
                      style={{ marginTop: 4, marginLeft: 5 }}
                    />
                  </TouchableOpacity>
                  }
                </View> 

                
            
          </View>

          <View style={styles.secondContainer}>
            <View style={styles.newJobContainer}>
              <View>
                <Text style={styles.newJob}>New Recommended Jobs</Text>
                <Text style={styles.jobHeading}>Professional, precise</Text>
                <Text style={styles.jobaddress}>Alex Software.ltd, USA</Text>
              </View>
              <View>
                <View style={styles.jobNumberView}>
                  <Text style={styles.jobNumber}>42</Text>
                </View>
              </View>
            </View>
            {/* <View style={{height:.5,width:"87%",backgroundColor:"#B2BABB"}}></View> */}
            <View style={styles.jobrecuiterView}>
              <Text style={styles.Recuiter}>New Jobs from Recruiters</Text>
              <Text style={styles.RecuiterNumber}>07</Text>
            </View>
            {/* <View style={{height:.8,width:"87%",backgroundColor:"#B2BABB"}}></View> */}
            <View style={styles.saveJobView}>
              <Text style={styles.saveJob}>Saved Jobs</Text>
              <Text style={styles.saveJobNumber}>12</Text>
            </View>
          </View>
      </View>
      </ScrollView>
    );
  }
}

export default EmployeeHomescreen;

const styles = StyleSheet.create({
  MainContainer: {
    width: screenwidth,
    //height: screenHeight,
    marginBottom: 100,

  },
  firstBodycontainer: {
    width: "100%",
    height: 193,
    borderColor: "#000",
    // shadowOpacity: 0.9,
    elevation: 1,
    // shadowOffset: {
    // width: 0,
    // height: 4
    // }
  },
  performenceContainer: {
    width: "85%",
    alignSelf: "center",
    marginTop: 10,
    marginBottom: 20,
  },
  performance: {
    color: "#4a4a4a",
    fontFamily: FontFamily.Poppins_Medium,
    fontSize: 14,
    //fontWeight: "600"
  },
  apperancesContainer: {
    flexDirection: "row",
    marginTop: 15,
    marginBottom: 25,
    width: "100%",
    justifyContent: "space-between",
  },
  apperancesNumber: {
    fontWeight: "600",
    fontSize: 30,
    color: "#000000",
    fontFamily: FontFamily.Poppins_Medium,
  },
  apperances: {
    //fontWeight:"600",
    fontSize: 14,
    color: "#000000",
    fontFamily: FontFamily.Poppins_Medium,
  },
  recuiterActionNumber: {
    fontWeight: "600",
    fontSize: 30,
    color: "#000000",
    fontFamily: FontFamily.Poppins_Medium,
  },
  recuiterAction: {
    //fontWeight:"bold",
    fontSize: 14,
    color: "#000000",
    fontFamily: FontFamily.Poppins_Medium,
  },
  missingProfileText: {
    color: "#384476",
    fontSize: 14,
    fontFamily: FontFamily.Poppins_Regular
  },
  secondContainer: {
    width: "100%",
    alignItems: "center",
    paddingTop: 30,
  },
  newJobContainer: {
    flexDirection: "row",
    width: "85%",
    justifyContent: "space-between",
  },
  newJob: {
    fontSize: 15,
    color: "#484848",
    marginBottom: 30,
  },
  jobHeading: {
    fontSize: 14,
    fontWeight: "bold",
    color: "#374271",
    marginBottom: 7
  },
  jobaddress: {
    fontSize: 11,
    color: "#4a4a4a",
    marginBottom: 40
  },
  jobNumberView: {
    backgroundColor: "#262e50",
    height: 55,
    width: screenwidth / 4,
    borderRadius: 100,
    justifyContent: "center",
    marginTop: 20
  },
  jobNumber: {
    fontSize: 30,
    alignSelf: "center",
    color: "#fff",
  },
  jobrecuiterView: {
    width: "85%",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  Recuiter: {
    fontSize: 15,
    color: "#484848"
  },
  RecuiterNumber: {
    fontSize: 22,
    color: "#262e50",
    marginRight: "8%"
  },
  saveJobView: {
    width: "85%",
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 15
  },
  saveJob: {
    fontSize: 15,
    color: "#484848"
  },
  saveJobNumber: {
    fontSize: 22,
    color: "#262e50",
    marginRight: "8%"
  }
})