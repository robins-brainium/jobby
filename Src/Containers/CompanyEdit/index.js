import React, { Component } from 'react';
import { TouchableOpacity, View, Text, StyleSheet, Dimensions, FlatList, TextInput, Image, ScrollView } from 'react-native';
import HeaderComponent from "../../Components/Header"
import TextInputComponent from '../../Components/Login/TextInput';
import companyPic from '../../Images/Company_Profile/company.png'
import phonecallPic from '../../Images/Company_Profile/phonecall.png'
import designationPic from '../../Images/Company_Profile/designation.png'
import ButtonComponent from '../../Components/Button/loginButton';
import FloatingLabelInput from '../../Components/FlotingTextInput';
import FontFamily from '../../Utils/FontFamily';
const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;


class CompanyEdit extends Component {

    state = {
        companyname: "Brainium Information Technology",
        address: "Washion DC",
        phoneNo: "9830731745",
        email: "loriem@mail.com",
        designation: "Loriem Ipsum Loriem Ipsum",
        country:"India",
        city:"kolkata",
        fname:"John",
        lname:"Doe",
        description:"Loriem Ipsum Loriem Ipsum Loriem Ipsum Loriem Ipsum"
    }

    render() {
        return (
            //Main Container
            <View style={styles.MainContainer}>
                <HeaderComponent
                    backicon
                    dot
                    noNotification
                    onClick2={() => this.props.navigation.goBack()}
                    headerTitle="Edit Company Details"
                />
                <View style={{ flex: 1, marginTop: 40 }} >
                    <ScrollView>
                        <View style={styles.repeatContainer}>
                            <FloatingLabelInput
                                textInputHeight={30}
                                textInputWidth={screenwidth / 1.25}
                                label="Company name"
                                value={this.state.companyname}
                                onChangeText={(val) => this.setState({ companyname: val })}
                                placeholder="Ex. Brainium Information Technology"
                            />
                        </View>

                        {/* <TextInputComponent
                            titleName={"Company Name"}
                            iconType={"image"}
                            width={16}
                            height={20}
                            image={companyPic}
                            placeholder={"Brainium Information Technology"}
                            onChangeText={(val) => this.setState({ companyname: val })}
                            value={this.state.companyname}
                        /> */}
                         <View style={styles.repeatContainer}>
                            <FloatingLabelInput
                                textInputHeight={30}
                                textInputWidth={screenwidth / 1.25}
                                label="Country"
                                value={this.state.country}
                                onChangeText={(val) => this.setState({ country: val })}
                                placeholder="Enter Country Name"
                            />
                        </View>
                        <View style={styles.repeatContainer}>
                            <FloatingLabelInput
                                textInputHeight={30}
                                textInputWidth={screenwidth / 1.25}
                                label="City"
                                value={this.state.city}
                                onChangeText={(val) => this.setState({ city: val })}
                                placeholder="Ex. Kolkata"
                            />
                        </View>
                        {/* <TextInputComponent
                            titleName={"City"}
                            iconType={"SimpleLineIcons"}
                            iconName={"location-pin"}
                            iconSize={18}
                            iconColor="black"
                            placeholder={"Washion Dc"}
                            onChangeText={(val) => this.setState({ city: val })}
                            value={this.state.city}
                        /> */}
                       
                        {/* <TextInputComponent
                            titleName={"Country"}
                            iconType={"SimpleLineIcons"}
                            iconName={"location-pin"}
                            iconSize={18}
                            iconColor="black"
                            placeholder={"Washion Dc"}
                            onChangeText={(val) => this.setState({ country: val })}
                            value={this.state.country}
                        /> */}
                         <View style={styles.repeatContainer}>
                            <FloatingLabelInput
                                textInputHeight={30}
                                textInputWidth={screenwidth / 1.25}
                                label="First Name"
                                value={this.state.first}
                                onChangeText={(val) => this.setState({ first: val })}
                                placeholder="First Name of the job poster"
                            />
                        </View>
                        {/* <TextInputComponent
                            titleName={"First Name"}
                            iconType={"MaterialIcons"}
                            width={16}
                            height={20}
                            iconName={"text-fields"}
                            placeholder={"First Name of the job poster"}
                            onChangeText={(val) => this.setState({ first: val })}
                        /> */}
                        <View style={styles.repeatContainer}>
                            <FloatingLabelInput
                                textInputHeight={30}
                                textInputWidth={screenwidth / 1.25}
                                label="Last Name"
                                value={this.state.last}
                                onChangeText={(val) => this.setState({ last: val })}
                                placeholder="Last Name of the job poster"
                            />
                        </View>

                        {/* <TextInputComponent
                            titleName={"Last Name"}
                            iconType={"MaterialIcons"}
                            width={16}
                            height={20}
                            iconName={"text-fields"}
                            placeholder={"Last Name of the job poster"}
                            onChangeText={(val) => this.setState({ last: val })}
                        /> */}

                        <View style={styles.repeatContainer}>
                            <FloatingLabelInput
                                textInputHeight={30}
                                textInputWidth={screenwidth / 1.25}
                                label="Email ID"
                                value={this.state.email}
                                onChangeText={(val) => this.setState({ email: val })}
                                placeholder="Enter Your Email Id"
                            />
                        </View>


                        {/* <TextInputComponent
                            titleName={"Email ID"}
                            iconType={"Octicons"}
                            iconName={"mail"}
                            iconSize={18}
                            iconColor="black"
                            placeholder={"Enter your email id"}
                            onChangeText={(val) => this.setState({ email: val })}
                            value={this.state.email}
                        /> */}
                        <View style={styles.repeatContainer}>
                        <Text style={styles.title}>Job Describtion</Text>
                        <TextInput
                            underlineColorAndroid="transparent"
                            placeholder="Job Description"
                            placeholderTextColor="#999"
                            autoCapitalize="none"
                            onChangeText={(val) => this.setState({ description: val })}
                            multiline={true}
                            value={this.state.description}
                            style={styles.mytextinput}
                        />
                        </View>
                        {/* <TextInputComponent
                            titleName={"Description"}
                            iconType={"image"}
                            width={16}
                            height={20}
                            image={designationPic}
                            placeholder={"Enter about your Company"}
                            onChangeText={(val) => this.setState({ designation: val })}
                            value={this.state.designation}
                        /> */}

                        <View style={{ marginBottom: 20, alignItems: "center", marginTop: 10 }}>
                            <ButtonComponent
                                buttonName="Save"
                            />
                        </View>
                    </ScrollView>
                </View>
            </View>
        );
    }
}

export default CompanyEdit;

const styles = StyleSheet.create({
    MainContainer: {
        //width: screenwidth,
        //height: screenHeight,
        flex: 1,
        marginBottom: 40
    },
    repeatContainer: {
        width: "80%", alignSelf: "center", marginBottom:10
      },
      mytextinput: {
        backgroundColor: "#fff", borderColor: "#999", borderWidth: 1, height: 80, width: "100%", paddingHorizontal: 10, textAlignVertical: 'top'
      },
      title:{
          marginBottom:10,
          fontSize:14,
          fontFamily:FontFamily.Poppins_Medium
      }
})