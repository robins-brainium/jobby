import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, TextInput, ScrollView, KeyboardAvoidingView, Platform } from 'react-native';
import HeaderComponent from "../../Components/Header"
import FontSize from '../../Utils/fonts';
import ButtonComponent from '../../Components/Button/loginButton'
import FontFamily from '../../Utils/FontFamily';
import Colors from '../../Utils/Colors';
import TextInputComponent from "./../../Components/Login/TextInput"
import { RadioButton } from 'react-native-paper';
import TagInput from 'react-native-tag-input';
import FloatingLabelInput from '../../Components/FlotingTextInput';
const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;


const horizontalInputProps = {
  keyboardType: 'default',
  returnKeyType: 'search',
  placeholder: 'Enter Tag',
  style: {
    fontSize: 12,
    marginVertical: Platform.OS == 'ios' ? -5 : -2,
    height:50,
    padding:0
  },
};

const horizontalScrollViewProps = {
  horizontal: true,
  showsHorizontalScrollIndicator: false,
};

class PostJob extends Component {

  state = {
    title: '',
    location: '',
    value: 'first',
    horizontalTags: [],
    horizontalText: "",
    description: '',
    companydetails: '',
    tagline: '',
    experience: '',
    city: '',
    country: '',
    company:"",
    remote:'internship',
    ar_title: '',

  }

  labelExtractor = (tag) => tag;

  onChangeHorizontalTags = (horizontalTags) => {
    this.setState({
      horizontalTags,
    });
  };

  onChangeHorizontalText = (horizontalText) => {
    this.setState({ horizontalText });

    const lastTyped = horizontalText.charAt(horizontalText.length - 1);
    const parseWhen = [',', ' ', ';', '\n'];

    if (parseWhen.indexOf(lastTyped) > -1) {
      this.setState({
        horizontalTags: [...this.state.horizontalTags, this.state.horizontalText],
        horizontalText: "",
      });
      this._horizontalTagInput.scrollToEnd();
    }
  }

  renderRadioCircleIos = (radio) => {
    Platform.OS === 'ios' 
    return( <View style={[{left: radio}, styles.radioCircle]}/> )
  }

  render() {
    const { navigation } = this.props;
    return (
      //Main Container
      <View style={styles.MainContainer}>
        <HeaderComponent
          drawericon
          onClick={() => navigation.openDrawer()}
          headerTitle="Post Job"
          // type={"textinput"}
          // onChangeText={(postjob)=> this.setState({postjob})}
          // placeholder="Search"
          notificationonpress={() => this.props.navigation.navigate("Notification")}
        />
        <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding" >
          <ScrollView
            contentContainerStyle={{ width: "100%", }}>
            <View style={{ alignItems: "center", marginBottom: 20, paddingTop: 30 }}>
              <Text style={styles.mainjobtitle}>Enter your job details</Text>
            </View>

            <View style={styles.repeatContainer}>
              <FloatingLabelInput
                textInputHeight={30}
                textInputWidth={screenwidth / 1.25}
                label="Company Name"
                value={this.state.company}
                onChangeText={(val) => this.setState({ company: val })}
                placeholder="Enter your Company Name"
              />
            </View>

            {/* <TextInputComponent
              titleName={"Company"}
              iconType={"MaterialIcons"}
              iconName={"title"}
              iconSize={20}
              iconColor="grey"
              placeholder={"Enter Company Name"}
              onChangeText={(val) => this.setState({ company: val })}
            /> */}
            <View style={styles.repeatContainer}>
              <FloatingLabelInput
                textInputHeight={30}
                textInputWidth={screenwidth / 1.25}
                label="Job Title"
                value={this.state.title}
                onChangeText={(val) => this.setState({ title: val })}
                placeholder="Enter your Job Title"
              />
            </View>

            <View style={styles.repeatContainer}>
              <FloatingLabelInput
                textInputHeight={30}
                textInputWidth={screenwidth / 1.25}
                label="عنوان وظيفي"
                value={this.state.ar_title}
                onChangeText={(val) => this.setState({ ar_title: val })}
                placeholder="أدخل عنوان الوظيفة العربية"
              />
            </View>
            {/* <TextInputComponent
              titleName={"JobTitle"}
              iconType={"MaterialIcons"}
              iconName={"title"}
              iconSize={20}
              iconColor="grey"
              placeholder={"Enter JobTitle"}
              onChangeText={(val) => this.setState({ title: val })}
            /> */}
            {/* <TextInputComponent
              titleName={"عنوان وظيفي"}
              iconType={"MaterialIcons"}
              iconName={"title"}
              iconSize={20}
              iconColor="grey"
              placeholder={"أدخل عنوان الوظيفة العربية"}
              onChangeText={(val) => this.setState({ ar_title: val })}
            /> */}
            <View style={styles.repeatContainer}>
              <FloatingLabelInput
                textInputHeight={30}
                textInputWidth={screenwidth / 1.25}
                label="Country"
                value={this.state.country}
                onChangeText={(val) => this.setState({ country: val })}
                placeholder="Enter your Country Name"
              />
            </View>

            {/* <TextInputComponent
              titleName={"Country"}
              iconType={"Octicons"}
              iconName={"location"}
              iconSize={18}
              width={16}
              height={20}
              iconColor="grey"
              placeholder={"Enter Your Country"}
              onChangeText={(val) => this.setState({ country: val })}
            /> */}
            <View style={styles.repeatContainer}>
              <FloatingLabelInput
                textInputHeight={30}
                textInputWidth={screenwidth / 1.25}
                label="City"
                value={this.state.city}
                onChangeText={(val) => this.setState({ city: val })}
                placeholder="Enter your City Name"
              />
            </View>
            {/* <TextInputComponent
              titleName={"City"}
              iconType={"Octicons"}
              iconName={"location"}
              iconSize={18}
              width={16}
              height={20}
              iconColor="grey"
              placeholder={"Enter Your City name"}
              onChangeText={(val) => this.setState({ city: val })}
            /> */}

            <View style={styles.repeatContainer}>
              <Text style={styles.title}>Application Job Type</Text>
              <View style={{ flexDirection: "row", flexWrap: "wrap", }}>
                <RadioButton.Group
                  onValueChange={value => this.setState({ value })}
                  value={this.state.value}>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>Fulltime</Text>
                    {this.renderRadioCircleIos('63%')}
                    <RadioButton value="first" />
                  </View>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>Part-time</Text>
                    {this.renderRadioCircleIos('70%')}
                    <RadioButton value="second" />
                  </View>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>Temporary</Text>
                    {this.renderRadioCircleIos('71%')}
                    <RadioButton value="temporary" />
                  </View>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>Volunteer</Text>
                    {this.renderRadioCircleIos('70%')}
                    <RadioButton value="volunteer" />
                  </View>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>Internship</Text>
                    {this.renderRadioCircleIos('71%')}
                    <RadioButton value="internship" />
                  </View>
                </RadioButton.Group>
              </View>
            </View>
            <View style={styles.repeatContainer}>
              <Text style={[styles.title]}>Add Skill</Text>
              <View style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: '#fff', borderBottomColor:"#999", borderBottomWidth:1, }}>
                <TagInput
                  ref={(horizontalTagInput) => { this._horizontalTagInput = horizontalTagInput }}
                  value={this.state.horizontalTags}
                  onChange={this.onChangeHorizontalTags}
                  labelExtractor={this.labelExtractor}
                  text={this.state.horizontalText}
                  onChangeText={this.onChangeHorizontalText}
                  tagColor="#05f7ef"
                  tagTextColor="#000"
                  inputProps={horizontalInputProps}
                  scrollViewProps={horizontalScrollViewProps}
                  tagContainerStyle={{height:35}}
                />
              </View>
              <Text style={{ fontSize: FontSize.micro_size, color: "#cacbcc"}}>Note: Add "," for adding multiple tag</Text>
            </View>
            {/* <View style={styles.repeatContainer}>
              <Text style={[styles.title, { marginBottom: 10, }]}>Feature Image</Text>
              <ButtonComponent
                buttonName="Upload Image"
                backgroundColor1="#1a2246"
                height={40}
                width="50%"
              // onClick={() => this.props.navigation.navigate("PostJob")}
              />
            </View> */}
            <View style={styles.repeatContainer}>
              <Text style={styles.title}>Job Describtion</Text>
              <TextInput
                underlineColorAndroid="transparent"
                placeholder="Job Description"
                placeholderTextColor="#999"
                autoCapitalize="none"
                onChangeText={(val) => this.setState({ description: val })}
                multiline={true}
                value={this.state.description}
                style={styles.mytextinput}
              />
            </View>
            {/* <View style={styles.repeatContainer}>
              <Text style={styles.title}>Company Details</Text>
              <TextInput
                underlineColorAndroid="transparent"
                placeholder="Company Details"
                placeholderTextColor="#999"
                autoCapitalize="none"
                onChangeText={(val) => this.setState({ companydetails: val })}
                multiline={true}
                value={this.state.companydetails}
                style={styles.mytextinput}
              />
            </View> */}
            <View style={styles.repeatContainer}>
              {/* <Text style={styles.title}>Company Industry</Text> */}
              <FloatingLabelInput
                textInputHeight={30}
                textInputWidth={screenwidth / 1.25}
                label="Company"
                value={this.state.companyIndustrial}
                onChangeText={(val) => this.setState({ companyIndustrial: val })}
                placeholder={"Enter Your Company Industry"}
              />
            </View>
            {/* <TextInputComponent
              titleName={"Company"}
              iconType={"MaterialIcons"}
              iconName={"description"}
              iconSize={18}
              width={16}
              iconColor="grey"
              placeholder={"Enter Your Company Industry"}
              onChangeText={(val) => this.setState({ tagline: val })}
            /> */}
            {/* <View style={styles.repeatContainer}>
              <Text style={styles.title}>Social Medialinks</Text>
            </View>
            <TextInputComponent
              titleName={"Social Media"}
              iconType={"MaterialIcons"}
              iconName={"description"}
              iconSize={18}
              width={16}
              iconColor="grey"
              placeholder={"Enter Social Media Link"}
              onChangeText={(val) => this.setState({ social: val })}
            /> */}

            {/* <View style={styles.repeatContainer}>
              <Text style={styles.title}>Company Logo</Text>
              <ButtonComponent
                buttonName="Choose Logo"
                backgroundColor1="#1a2246"
                height={40}
                width="50%"
              // onClick={() => this.props.navigation.navigate("PostJob")}
              />
            </View> */}
            <View style={styles.repeatContainer}>
              <Text style={styles.title}>Seniority Level</Text>
              <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                <RadioButton.Group
                  onValueChange={value => this.setState({ remote: value })}
                  value={this.state.remote}>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>Internship</Text>
                    {this.renderRadioCircleIos('71%')}
                    <RadioButton value="internship" />
                  </View>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>Entry-Level</Text>
                    {this.renderRadioCircleIos('72%')}
                    <RadioButton value="entry" />
                  </View>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>Associate</Text>
                    {this.renderRadioCircleIos('71%')}
                    <RadioButton value="associate" />
                  </View>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>Mid Senior Level</Text>
                    {this.renderRadioCircleIos('78%')}
                    <RadioButton value="senior" />
                  </View>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>Director</Text>
                    {this.renderRadioCircleIos('68%')}
                    <RadioButton value="director" />
                  </View>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>Executive</Text>
                    {this.renderRadioCircleIos('71%')}
                    <RadioButton value="executive" />
                  </View>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>Non-Applicable</Text>
                    {this.renderRadioCircleIos('78%')}
                    <RadioButton value="non-applicable" />
                  </View>
                </RadioButton.Group>
              </View>
            </View>
            {/* <View style={styles.repeatContainer}>
              <Text style={styles.title}>Is position remote?</Text>
              <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                <RadioButton.Group
                  onValueChange={value => this.setState({ remote: value })}
                  value={this.state.remote}>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>Yes</Text>
                    {this.renderRadioCircleIos('47%')}
                    <RadioButton value="yes" />
                  </View>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>No</Text>
                    {this.renderRadioCircleIos('45%')}
                    <RadioButton value="no" />
                  </View>
                </RadioButton.Group>
              </View>
            </View> */}



            {/* <View style={styles.repeatContainer}>
              <Text style={styles.title}>Is position Onsite?</Text>
              <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                <RadioButton.Group
                  onValueChange={value => this.setState({ onsite: value })}
                  value={this.state.onsite}>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>Yes</Text>
                    {this.renderRadioCircleIos('47%')}
                    <RadioButton value="yes" />
                  </View>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>No</Text>
                    {this.renderRadioCircleIos('45%')}
                    <RadioButton value="no" />
                  </View>
                </RadioButton.Group>
              </View>
            </View> */}

            <View style={styles.repeatContainer}>
              {/* <Text style={styles.title}>Year of Experience</Text> */}
              <FloatingLabelInput
                textInputHeight={30}
                textInputWidth={screenwidth / 1.25}
                label="Experience"
                value={this.state.experience}
                onChangeText={(val) => this.setState({ experience: val })}
                placeholder={"Year of Experience (only numbers)"}
                keyboardType="numeric"
              />
            </View>
            {/* <TextInputComponent
              titleName={"Experience"}
              iconType={"MaterialIcons"}
              iconName={"description"}
              iconSize={18}
              width={16}
              iconColor="grey"
              placeholder={"Years of Experience"}
              onChangeText={(val) => this.setState({ experience: val })}
              keyboardType="numeric"
            /> */}

            <View style={{ flexDirection: "row", justifyContent: 'space-around', marginBottom: 40 }}>
              <ButtonComponent
                buttonName="Submit Details"
                backgroundColor1="#73e470"
                height={50}
                width="80%"
                onClick={() => this.props.navigation.navigate("PostJob")}
              />
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

export default PostJob;

const styles = StyleSheet.create({
  MainContainer: {
    width: screenwidth,
    height: screenHeight,
    backgroundColor:"#fff"
  },
  repeatContainer: {
    width: "80%", alignSelf: "center", marginBottom:10
  },
  mytextinput: {
    backgroundColor: "#fff", borderColor: "#999", borderWidth: 1, height: 80, width: "100%", paddingHorizontal: 10, textAlignVertical: 'top'
  },
  title: {
    fontSize: 14, fontFamily: FontFamily.Poppins_Medium
  },
  mainjobtitle: {
    fontSize: FontSize.medium_size, fontFamily: "Poppins-Medium", color: Colors.BlackColor
  },
  radioCircle: {
    padding: 9, 
    borderWidth: 2, 
    borderColor: Colors.Nero, 
    borderRadius: 100, 
    position: 'absolute'
  }
})