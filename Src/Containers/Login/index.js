import React, { Component } from 'react';
import { View, Text, ImageBackground, StyleSheet, StatusBar, Dimensions, Image, TouchableOpacity, Platform } from 'react-native';
import Colors from "./../../Utils/Colors";
import FontSize from "./../../Utils/fonts";
import TextInputComponent from "./../../Components/Login/TextInput"
import ButtonComponent from './../../Components/Button/loginButton'
import AsyncStorage from '@react-native-community/async-storage';
import { RadioButton } from 'react-native-paper';
import FontFamily from '../../Utils/FontFamily';
import Apis from '../../Network/apicall';
import Toast from 'react-native-simple-toast';
import FloatingLabelInput from '../../Components/FlotingTextInput';
import {save_user} from '../../actions/userAction';
import {connect} from 'react-redux';

const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;


class Login extends Component {


  state = {
    passIcon: true,
    email: "",
    password: "",
    storage: "",
    type:"Employer"
  }

  async componentDidMount() {
    const storage = await AsyncStorage.getItem('usertype');
    this.setState({
      storage: storage
    })
  }

  // <======= Password show and hide functionlity =========>
  handleIconChange() {
    if (this.state.passIcon == true) {
      this.setState({ passIcon: false })
    } else {
      this.setState({ passIcon: true })
    }
  }

  // <======= for radio button cicle =========>
  renderRadioCircleIos = (radio) => {
    Platform.OS === 'ios' 
    return( <View style={[{left: radio}, styles.radioCircle]}/> )
  }

  // <======= Email Validation =========>
  validate = (text) => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      // Toast.show('Email is not Correct');
      this.setState({ email: "incorrect" })
      return false;
    }
    else {
      this.setState({ email: text })
      // Toast.show('Email is Correct');
    }
  }

  // <======= Employer Login api all =========>
  employer_login = () => {
    if(this.state.email == '' || this.state.password == ''){
      Toast.show('Field Cannot be blanked');
    }
    else if(this.state.email == "incorrect") {
      Toast.show('Email is incorrect');
    }
    else {
      const apidata = {
        "email": this.state.email,
        "password": this.state.password,
        "devicetoken": 123,
        "apptype": Platform.OS === 'android' ? "ANDROID" : 'IOS'
      }      
      Apis.employer_login(apidata)
      .then((res)=>{
        if(res.response_code == 2000){
         this.setdata(res.response_data)
          console.log(res.response_data, "response data");
          Toast.show("Success  " + res.response_message);
          //this.props.navigation.navigate('EmployeeHomescreen');
        }
        else{
         Toast.show("Info  " + res.response_message);
        }
      })
      .catch((error) => {
        console.error(error);
      });
    }
  }

  // <======= Employee Login api all =========>
  employee_login = () => {
   if(this.state.email == '' || this.state.password == ''){
     Toast.show('Field Cannot be blanked');
   }
   else if(this.state.email == "incorrect") {
     Toast.show('Email is incorrect');
   }
   else {
     const apidata = {
       "email": this.state.email,
       "password": this.state.password,
       "devicetoken": 123,
       "apptype": Platform.OS === 'android' ? "ANDROID" : 'IOS'
     }
     Apis.employee_login(apidata)
     .then((res)=>{
       if(res.response_code == 2000){
         this.setdata(res.response_data)
         console.log(res.response_data, "response data");
         Toast.show("Success  " + res.response_message);
         //this.props.navigation.navigate('EmployeeHomescreen');
       }
       else{
        Toast.show("Info  " + res.response_message);
       }
     })
     .catch((error) => {
       console.error(error);
     });
   }
 }

 setdata = async (res) => {
  await AsyncStorage.setItem("authtocken", res.authtoken);
  this.props.save_user(res.authtoken)
  await AsyncStorage.setItem("usertype", res.user_type);

 }


  render() {
    return (
      <ImageBackground style={styles.container}
        source={require("../../Images/Login/login_bckground.png")}
      >
        <StatusBar backgroundColor={Colors.StatusBar} barStyle="light-content" />
        <View style={styles.logo_container}>
          <Image source={require("./../../Images/Login/logo.png")}
            style={styles.logo_image}
          />
        </View>
        <View style={styles.repeatContainer}>
          <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
            <RadioButton.Group
              onValueChange={value => this.setState({
                type: value
              })}
              value={this.state.type}>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={{ fontSize: 14, fontFamily: FontFamily.Poppins_Regular }}>Employer</Text>
                {this.renderRadioCircleIos('70%')}
                <RadioButton value="Employer" />
              </View>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={{ fontSize: 14, fontFamily: FontFamily.Poppins_Regular }}>Employee</Text>
                {this.renderRadioCircleIos('72%')}
                <RadioButton value="Employee" />
              </View>
            </RadioButton.Group>
          </View>
        </View>
        <View style={[styles.repeatContainer]}>
          <FloatingLabelInput
            textInputHeight={30}
            textInputWidth={"100%"}
            label="Email"
            // value={this.state.email}
            onChangeText={(val) =>  this.validate(val)}
            placeholder={"Enter your email"}
            autoCapitalize="none"
          />
        </View>
        {/* <TextInputComponent
          titleName={"Email"}
          iconType={"Octicons"}
          iconName={"mail"}
          iconSize={18}
          iconColor="grey"
          rightIconType={"MaterialIcons"}
          placeholder={"Enter your email"}
          onChangeText={(val) => this.validate(val)}
        /> */}
         <View style={[styles.repeatContainer]}>
          <FloatingLabelInput
            textInputHeight={30}
            textInputWidth={"100%"}
            label="Password"
            value={this.state.password}
            onChangeText={(val) =>  this.setState({ password: val })}
            placeholder={"Enter your Password"}
            secureTextEntry={true}
          />
        </View>
        {/* <TextInputComponent
          iconName={"lock"}
          iconType={"SimpleLineIcons"}
          iconSize={18}
          iconColor="grey"
          titleName={"Password"}
          rightIconType={"Ionicons"}
          placeholder={"Enter your password"}
          secureTextEntry={this.state.passIcon}
          passwordIconName={this.state.passIcon}
          handleIconChange={() => this.handleIconChange()}
          onChangeText={(val) => this.setState({ password: val })}
        /> */}

        <TouchableOpacity
          style={{ marginBottom: 25 }}
          onPress={() => this.props.navigation.navigate("ForgetPassword")}>
          <Text style={{ textAlign: "center", fontSize: FontSize.small_size }}>Forget pasword</Text>
        </TouchableOpacity>
        { this.state.type == "Employer" ?
        <View style={{ marginBottom: 20, alignItems: "center" }}>
          <ButtonComponent
            backgroundColor1="#1a2246"
            height={50}
            width="80%"
            buttonName="Sign In"
            onClick={() => this.employer_login()}
            // onClick={() => {this.props.navigation.navigate("DrawerComponent"), AsyncStorage.setItem("usertype", "employers")}}
          />
        </View>
        :
        <View style={{ marginBottom: 20, alignItems: "center" }}>
          <ButtonComponent
            buttonName="Sign In"
            backgroundColor1="#1a2246"
            height={50}
            width="80%"
            onClick={() => this.employee_login()}
            // onClick={() => {this.props.navigation.navigate("DrawerComponent"),
            // AsyncStorage.setItem("usertype", "employees")}}
          />
        </View>
        }
        <View
          style={{ alignItems: "center", justifyContent: "center", flexDirection: "row" }}>
          <Text style={styles.newUser_Text}>New User? </Text>
          <TouchableOpacity onPress={() => this.props.navigation.navigate("Landing")}>
            <Text style={{ color: "#1a2246", fontSize: FontSize.small_size }}> Sign Up</Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    );
  }
}



export default connect(null, {save_user})(Login)

const styles = StyleSheet.create({
  container: {
    width: screenwidth,
    height: screenHeight
  },
  logo_container: {
    alignItems: "center",
    height: screenHeight / 3,
    justifyContent: "center"
  },
  logo_image: {
    height: 39,
    width: 174
  },
  newUser_Text: {
    color: "#696969",
    fontSize: FontSize.small_size
  },
  SignUp_text: {
    color: "#1a2245",
    fontSize: FontSize.small_size
  },
  repeatContainer: {
    width: "80%", alignSelf: "center", marginBottom: 15
  },
  radioCircle: {
    padding: 9, 
    borderWidth: 2, 
    borderColor: Colors.Nero, 
    borderRadius: 100, 
    position: 'absolute'
  },
});