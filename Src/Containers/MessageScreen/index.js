import React, { Component } from 'react';
import { TouchableOpacity, View, Text, StyleSheet, Dimensions, FlatList, TextInput, Image } from 'react-native';
import HeaderComponent from "../../Components/Header"
import ContactList from '../../Components/ContactList';
import FontSize from '../../Utils/fonts';
import FontFamily from '../../Utils/FontFamily';
// import {  } from 'react-native-gesture-handler';
import MessageList from '../../Components/MessageList'

const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;



const MessageData = [
  {
    id: 1,
    name: "Stuart Alexandra",
    jobDescription: "Hiring for React Native Developer",
    image: require('../../Images/EmployerProfile/image.jpg'),
    companyName: "Brainium Information Technologies Pvt. Ltd."
  },
  {
    id: 2,
    name: "Stuart Alexandra",
    jobDescription: "Hiring for React Native Developer",
    image: require('../../Images/EmployerProfile/image.jpg'),
    companyName: "Brainium Information Technologies Pvt. Ltd."
  },
  {
    id: 3,
    name: "Stuart Alexandra",
    jobDescription: "Hiring for React Native Developer",
    image: require('../../Images/EmployerProfile/image.jpg'),
    companyName: "Brainium Information Technologies Pvt. Ltd."
  },
  {
    id: 4,
    name: "Stuart Alexandra",
    jobDescription: "Hiring for React Native Developer",
    image: require('../../Images/EmployerProfile/image.jpg'),
    companyName: "Brainium Information Technologies Pvt. Ltd."
  },
  {
    id: 5,
    name: "Stuart Alexandra",
    jobDescription: "Hiring for React Native Developer",
    image: require('../../Images/EmployerProfile/image.jpg'),
    companyName: "Brainium Information Technologies Pvt. Ltd."
  },
  {
    id: 6,
    name: "Stuart Alexandra",
    jobDescription: "Hiring for React Native Developer",
    image: require('../../Images/EmployerProfile/image.jpg'),
    companyName: "Brainium Information Technologies Pvt. Ltd."
  },
];

class MessageScreen extends Component {

  state = {
  }

  render() {
    return (
      <View style={styles.MainContainer}>
        <HeaderComponent
          backicon
          noNotification
          onClick2={() => this.props.navigation.goBack()}
          headerTitle="Message List"
        />
        <View style={{ flex: 1, paddingVertical:10}}>
          <FlatList
            data={MessageData}
            renderItem={({ item }) => (
              <MessageList
                id={item.id}
                name={item.name}
                jobDescription={item.jobDescription}
                image={item.image}
                companyName={item.companyName}
              />
            )}
            keyExtractor={item => item.id}
            showsVerticalScrollIndicator={false}
          />

        </View>
      </View>
    );
  }
}

export default MessageScreen;

const styles = StyleSheet.create({
  MainContainer: {
    //width: screenwidth,
    //height: screenHeight,
    flex: 1,
    marginBottom: 40
  },


})