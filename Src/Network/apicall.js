import { Network } from "."

//Network will recieve 4 Arguments
// "method(type of request)",
// "endpoint ()", 
// "body (if POST method)"
// See the main function at ./network.js

export default class Apis {
  // <========================= Employer All api call section =========================>
  static employer_registration = (data) => {
    return Network('POST', 'employers/register', data)
  }

  static employer_email_verification = (data) => {
    return Network('POST', 'employers/emailVerification', data)
  }

  static employer_login = (data) => {
    return Network('POST', 'employers/login', data)
  }

  static employer_forgotpassword = (data) => {
    return Network('POST', 'employers/forgotPassword', data)
  }

  static employer_resetpassword = (data) => {
    return Network('POST', 'employers/resetPassword', data)
  }
  
  static company_list = () => {
    return Network('GET', 'employers/company-list')
  }
  
  static SearchCompany = (page, limit, name) => {
    return Network('GET', 'employers/company-list?page=' + `${page}` + '&limit=' + `${limit}` + '&name=' + `${name}`)
  }


  // <========================= Employee All api call section =========================>
  static employee_login = (data) => {
    return Network('POST', 'employees/login', data)
  }

  static employee_forgotpassword = (data) => {
    return Network('POST', 'employees/forgotPassword', data)
  }

  static employee_resetpassword = (data) => {
    return Network('POST', 'employees/resetPassword', data)
  }

  static employee_registration = (data) => {
    return Network('POST', 'employees/register', data)
  }

  static employee_email_verification = (data) => {
    return Network('POST', 'employees/emailVerification', data)
  }

  // <========================= Common api call section =========================>

  static job_role = () => {
    return Network('GET', 'job-role-list')
  }

}
