import { SAVE_USER } from './types';

export const save_user = (data) => {
  console.log(data, "actions");
  return async dispatch => {
    dispatch({
        type: SAVE_USER,
        payload: data
    })
  }
}