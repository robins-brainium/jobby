const FontFamily = {
    Poppins_Medium: "Poppins-Medium",
    Poppins_Semibold: "Poppins-Semibold",
    Poppins_Regular: "Poppins-Regular",
    Poppins_Light: "Poppins-Light",
};

export default FontFamily;