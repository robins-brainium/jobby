const Colors = {
    StatusBar: "#0f5054",
    BlackColor: "#000000",
    DimGray:"#656565",
    Nero:"#242424",
    DrawerBackground: "#c9c9c9",
    WhiteColor: "#fff",
    DrawerUserColor: "#191919",
    logOut: "#1a2245",
    lightgray:"#494949",
};


export default Colors;

