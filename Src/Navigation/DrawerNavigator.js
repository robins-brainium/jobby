import React, { useEffect, useState } from 'react'
import { createDrawerNavigator } from '@react-navigation/drawer';
import EmployerRegistration from '../Containers/EmployerRegistration';
import EditJob from '../Containers/EditJob'
import Notification from "../Containers/Notifications"
import Help from '../Containers/Help';
//import JobListing from '../Containers/JobListing';
import EmployerProfile from '../Containers/EmployerProfile';
import DrawerContent from "./DrawerContent"
import Application from '../Containers/Application';
import PostJob from '../Containers/PostJob';
import EmployeeList from '../Containers/employeeList';
import AppliedProfile from '../Containers/AppliedProfile';
import Employee from '../Containers/Employee';
import ViewJob from '../Containers/ViewJob';
import ShortLised from '../Containers/ShortListed';
import ShortList from '../Containers/ShortList';
import Share from '../Containers/ShareApp'
import EmployeeHomescreen from '../Containers/EmployeeHomescreen'
import Terms from '../Containers/Terms';
import Setting from '../Containers/Setting';
import PrivacyPolicy from '../Containers/PrivacyPolicy';
import ReportBlock from '../Containers/BlockReport'
import CalenderSchedule from '../Containers/CalenderSchedule';
import MessageScreen from '../Containers/MessageScreen'
import Profile from '../Containers/Profile';
import EditProfile from '../Containers/EditProfile';
import ChangePassword from '../Containers/ChangePassword';
import { from } from 'rxjs';
import CompanyEdit from '../Containers/CompanyEdit';
import ManagePlan from '../Containers/ManagePlan';
import EmployerCompanyDetails from '../Containers/EmployerCompanyDetails'
import MainFrom from '../Containers/MainFrom';
import AsyncStorage from '@react-native-community/async-storage';
import { localeData } from 'moment';
import EmployerJobListing from '../Containers/EmployerJobListing'
import EmployeeJobList from '../Containers/EmployeeJobList'
import Employer from "../Containers/Employer"
import SubmitResume from '../Containers/SubmitResume'
import CompanyDetails from '../Containers/CompanyDetails';
import SaveJob from '../Containers/SaveJob'
import JobApplied from '../Containers/JobApplied'
import ApplyJob from '../Containers/JobApply'
import ProfileEmployer from '../Containers/ProfileEmployer'
import JobShortlisted from '../Containers/JobShortListed'
import AppliedJob from '../Containers/AppliedJob';
import Employers from '../Containers/Employers';
import SaveJobListing from '../Containers/SaveJobListing';
import Opening from '../Containers/Opening';


const Drawer = createDrawerNavigator();

const DrawerComponent = () => {
    const [useData, setData] = useState('');
    useEffect(() => {
        localeData();
    }, []);
    const localeData = async () => {
        const user = await AsyncStorage.getItem('usertype');
        setData(user);
    }
    return (
        <Drawer.Navigator drawerContent={props => DrawerContent(props, useData)}>
            <Drawer.Screen name="EmployeeHomescreen" component={EmployeeHomescreen} initialRouteName="EmployeeHomescreen" />
            <Drawer.Screen name="MainFrom" component={MainFrom} />
            <Drawer.Screen name="EmployerCompanyDetails" component={EmployerCompanyDetails} />
            <Drawer.Screen name="CompanyEdit" component={CompanyEdit} />
            <Drawer.Screen name="ManagePlan" component={ManagePlan} />
            <Drawer.Screen name="ChangePassword" component={ChangePassword} />
            <Drawer.Screen name="Profile" component={Profile} />
            <Drawer.Screen name="EditProfile" component={EditProfile} />
            <Drawer.Screen name="MessageScreen" component={MessageScreen} />
            <Drawer.Screen name="ReportBlock" component={ReportBlock} />
            <Drawer.Screen name="AppliedProfile" component={AppliedProfile} />
            <Drawer.Screen name="PostJob" component={PostJob} />
            <Drawer.Screen name="ViewJob" component={ViewJob} />
            <Drawer.Screen name="ShortList" component={ShortList} />
            <Drawer.Screen name="Terms" component={Terms} />
            <Drawer.Screen name="PrivacyPolicy" component={PrivacyPolicy} />
            <Drawer.Screen name="Setting" component={Setting} />
            <Drawer.Screen name="Share" component={Share} />
            <Drawer.Screen name="ShortLised" component={ShortLised} />
            <Drawer.Screen name="Employee" component={Employee} />
            <Drawer.Screen name="Application" component={Application} />
            <Drawer.Screen name="EmployeeList" component={EmployeeList} />
            <Drawer.Screen name="EmployerProfile" component={EmployerProfile} />
            <Drawer.Screen name="CalenderSchedule" component={CalenderSchedule} />
            <Drawer.Screen name="Help" component={Help} />
            <Drawer.Screen name="EditJob" component={EditJob} />
            <Drawer.Screen name="Notification" component={Notification} />
            <Drawer.Screen name="EmployerJobListing" component={EmployerJobListing} />
            <Drawer.Screen name="EmployeeJobList" component={EmployeeJobList} />
            <Drawer.Screen name="Employer" component={Employer} />
            <Drawer.Screen name="SubmitResume" component={SubmitResume} />
            <Drawer.Screen name="CompanyDetails" component={CompanyDetails} />
            <Drawer.Screen name="SaveJob" component={SaveJob} />
            <Drawer.Screen name="JobApplied" component={JobApplied} />
            <Drawer.Screen name="ApplyJob" component={ApplyJob} />
            <Drawer.Screen name="AppliedJob" component={AppliedJob} />
            <Drawer.Screen name="Employers" component={Employers} />
            <Drawer.Screen name="SaveJobListing" component={SaveJobListing} />
            <Drawer.Screen name="ProfileEmployer" component={ProfileEmployer} />
            <Drawer.Screen name="JobShortlisted" component={JobShortlisted} />
            <Drawer.Screen name="Opening" component={Opening} />
            
        </Drawer.Navigator>
    )
}

export default DrawerComponent;
