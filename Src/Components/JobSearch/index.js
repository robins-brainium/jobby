import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, FlatList, Image, TouchableOpacity, ScrollView } from 'react-native';
import HeaderComponent from "../../Components/Header"
import FontSize from '../../Utils/fonts';
import ButtonComponent from '../../Components/Button/loginButton'
import FontFamily from '../../Utils/FontFamily';
import Colors from '../../Utils/Colors';



const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;

const JobSearch = (props) => {

    return (
        <View style={{ flex: 1 }}>

            {/* //1st view */}
            <View style={styles.firstbody}>
                {/* <View style={styles.repeatbody}> */}
                    {props.headerName ?
                        <View style={{ marginBottom: 10 }}>
                            <Text style={styles.Headline}>{props.headerName}</Text>
                        </View>
                        : null}
                    <View style={styles.firstcontainer}>

                        <View style={styles.leftside}>
                            <Text style={styles.leftsidetext}>Designation</Text>
                        </View>
                        <View style={styles.meddleside}>
                            <Text>:</Text>
                        </View>
                        <View style={styles.rightside}>
                            <Text style={styles.textrightside}>{props.Designation}</Text>
                        </View>
                    </View>
                    <View style={styles.firstcontainer}>
                        <View style={styles.leftside}>
                            <Text style={styles.leftsidetext}>Location</Text>
                        </View>
                        <View style={styles.meddleside}>
                            <Text>:</Text>
                        </View>
                        <View style={styles.rightside}>
                            <Text style={styles.textrightside}>{props.Location}</Text>
                        </View>
                    </View>
                    <View style={styles.firstcontainer}>
                        <View style={styles.leftside}>
                            <Text style={styles.leftsidetext}>Experience</Text>
                        </View>
                        <View style={styles.meddleside}>
                            <Text>:</Text>
                        </View>
                        <View style={styles.rightside}>
                            <Text style={styles.textrightside}>{props.Experience}</Text>
                        </View>
                    </View>
                    <View style={styles.firstcontainer}>
                        <View style={styles.leftside}>
                            <Text style={styles.leftsidetext}>Key Skills</Text>
                        </View>
                        <View style={styles.meddleside}>
                            <Text>:</Text>
                        </View>
                        <View style={styles.rightside}>
                            <Text style={styles.textrightside}>{props.Skills}</Text>
                        </View>
                    </View>

                {/* </View> */}
            </View>

            {/* 2nd view */}
            <View style={styles.secondbody}>
               
                    <Text style={styles.aboutcompanytitle}>Job Description</Text>
                    <View style={{maxHeight:115}}>
                    <ScrollView showsVerticalScrollIndicator={false} 
                    scrollEnabled={false}contentContainerStyle={{ paddingVertical: 10 }}>
                       
                        <Text style={styles.cardText}>
                            {props.Description}
                        </Text>
                        </ScrollView>
                        <TouchableOpacity style={{ paddingVertical: 2 }}>
                            <Text style={styles.ReadMore}>Read Mode</Text>
                        </TouchableOpacity>
                        
                    
                    </View>
                
            </View>

            <View style={{ flex: 0.80, width:"100%",  justifyContent: 'center', alignItems: 'center', paddingTop: 15, }}>
                {props.firstButton||props.secondButton ?
              <View style={{flexDirection: "row",justifyContent: 'space-around',marginBottom:20}}>
                
                <ButtonComponent
                    marginRightSpace={10}
                    buttonName={props.firstButton}
                    backgroundColor1= {props.firstButtonColor}
                    height={50}
                    width="40%"
                    onClick={()=> props.applied()}
                />

                <ButtonComponent
                    buttonName={props.secondButton}
                    backgroundColor1= {props.secondButtonColor}
                    height={50}
                    width="40%"
                    //onClick={() => this.props.navigation.navigate("EmployerJobListing")}
                />
            </View>
            :null}
            {props.thirdButton ?
            <View style={{alignItems:"center",width:"80%",}}>
            <ButtonComponent
                    buttonName={props.thirdButton}
                    backgroundColor1= {props.thirdButtonColor}
                    height={50}
                    width="100%"
                    onClick={() => props.canclejob()}
                />
            </View>
            :null}
            {props.fourthButton ?
            <View style={{alignItems:"center",width:"80%",marginBottom:20}}>
            <ButtonComponent
                    buttonName={props.fourthButton}
                    backgroundColor1= {props.fourthButtonColor}
                    height={50}
                    width="100%"
                    // onClick={() => this.props.navigation.navigate("EmployerJobListing")}
                />
            </View>
            :null}
          </View>
        </View>
    );

}

export default JobSearch;

const styles = StyleSheet.create({

    cardText: {
        color: "#333333",
        fontFamily: FontFamily.Poppins_Light,
        //lineHeight: 19,
        textAlign:'justify',
        fontSize: 14
    },
    firstbody: {
       // flex: 0.32,
        //borderBottomWidth:0.2,
        borderColor:"grey",
        paddingHorizontal:20,
        elevation:1,
        paddingVertical: 15
    },
    secondbody: {
        backgroundColor: "#e9e9e9",
        //marginTop:25,
        marginBottom:0,
        //flex: 0.33,
        paddingHorizontal:18,
        // borderBottomWidth:0.3,
        borderColor:"grey",
        paddingBottom: 18,
        elevation: 1,
        paddingTop:20
    },
    Headline: {
        fontSize: FontSize.medium_size,
        fontFamily: FontFamily.Poppins_Medium
    },
    firstcontainer: { 
        flexDirection: "row", 
        marginBottom: 7 
    },
    aboutcompanytitle: {  
        fontSize: FontSize.medium_size, 
        fontFamily: FontFamily.Poppins_Medium 
    },
    leftside: { 
        flex: 0.35, 
        justifyContent: "flex-start" 
    },
    leftsidetext: { 
        fontSize: 14, 
        fontFamily: FontFamily.Poppins_Semibold, fontWeight: "600", 
    },
    rightside: { 
        flex: 0.62 
    },
    textrightside: { 
        fontSize: 14, 
        fontFamily: FontFamily.Poppins_Semibold, fontWeight: "600", 
        color: Colors.lightgray 
    },
    meddleside: {
        flex: 0.08, 
        justifyContent: "flex-start"
    },
    ReadMore: {
        color:"#000000"
    },
})