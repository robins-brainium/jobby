import React, { Component } from 'react';
import { View, Text, StyleSheet, Dimensions, FlatList, Image, TouchableOpacity, ScrollView, Button} from 'react-native';
import HeaderComponent from "../../Components/Header"
import FontSize from '../../Utils/fonts';
import ButtonComponent from '../../Components/Button/loginButton'
import FontFamily from '../../Utils/FontFamily';
import Colors from '../../Utils/Colors';



const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;

const ProfileEmpJob = (props) => {


    return (
        <View style={{ flex: 1 }}>

            {/* //1st view */}
            <View style={styles.firstbody}>
                {/* <View style={styles.repeatbody}> */}
                <View style={{ marginBottom: 10 }}>
                    <Text style={styles.Headline}>{props.headerName}</Text>
                </View>
                <View style={styles.firstcontainer}>

                    <View style={styles.leftside}>
                        <Text style={styles.leftsidetext}>Designation</Text>
                    </View>
                    <View style={styles.meddleside}>
                        <Text>:</Text>
                    </View>
                    <View style={styles.rightside}>
                        <Text style={styles.textrightside}>{props.Designation}</Text>
                    </View>
                </View>
                <View style={styles.firstcontainer}>
                    <View style={styles.leftside}>
                        <Text style={styles.leftsidetext}>Company</Text>
                    </View>
                    <View style={styles.meddleside}>
                        <Text>:</Text>
                    </View>
                    <View style={styles.rightside}>
                        <Text style={styles.textrightside}>{props.CompanyName}</Text>
                    </View>
                </View>
                <View style={styles.firstcontainer}>
                    <View style={styles.leftside}>
                        <Text style={styles.leftsidetext}>Experience</Text>
                    </View>
                    <View style={styles.meddleside}>
                        <Text>:</Text>
                    </View>
                    <View style={styles.rightside}>
                        <Text style={styles.textrightside}>{props.Experience}</Text>
                    </View>
                </View>
                <View style={styles.firstcontainer}>
                    <View style={styles.leftside}>
                        <Text style={styles.leftsidetext}>Total Exp.</Text>
                    </View>
                    <View style={styles.meddleside}>
                        <Text>:</Text>
                    </View>
                    <View style={styles.rightside}>
                        <Text style={styles.textrightside}>{props.TotalExperience}</Text>
                    </View>
                </View>
                <View style={styles.firstcontainer}>
                    <View style={styles.leftside}>
                        <Text style={styles.leftsidetext}>Key Skills</Text>
                    </View>
                    <View style={styles.meddleside}>
                        <Text>:</Text>
                    </View>
                    <View style={styles.rightside}>
                        <Text style={styles.textrightside}>{props.Skills}</Text>
                    </View>
                </View>

                {/* </View> */}
            </View>

            {/* 2nd view */}
            <View style={styles.secondbody}>

                <View style={{ width: "95%",alignSelf:"center",marginVertical:20}}>
                    <View style={{ flexDirection: "row", justifyContent: "space-between", }}>
                      <TouchableOpacity style={{width:"58%"}}>
                        <View style={{alignItems:"center",width:"100%",borderRadius:80,height:"100%",height:40,justifyContent:"center",elevation:2}}>
                       <Text style={{color:"#ff0030"}}>schedule a video interview</Text>
                        </View>
                      </TouchableOpacity>
                      
                      <TouchableOpacity style={{width:"38%"}}>
                        <View style={{elevation:2, alignItems:"center",width:"100%",borderRadius:80, height:40,justifyContent:"center"}}>
                        <Text style={{color:"#3e869d"}}>Upload Resumes</Text>
                        
                        </View>
                      </TouchableOpacity>
                    </View>
                </View>

                <Text style={styles.aboutcompanytitle}>Profile Description</Text>
                <View style={{ maxHeight: 70 }}>
                    <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{ paddingVertical: 10 }}>

                        <Text style={styles.cardText}>
                            {props.Description}
                        </Text>
                    </ScrollView>
                </View>
            </View>

            <View style={{ flex: 0.15, width: "100%", justifyContent: 'space-around', alignItems: 'center', paddingTop: 15, marginTop:10}}>
                <View style={{ flexDirection: "row", justifyContent: 'space-around', }}>

                    <ButtonComponent
                        marginRightSpace={10}
                        buttonName={props.firstButton}
                        backgroundColor1={props.firstButtonColor}
                        height={50}
                        width="40%"
                        onClick={() => this.props.navigation.navigate("PostJob")}
                    />

                    <ButtonComponent
                        buttonName={props.secondButton}
                        backgroundColor1={props.secondButtonColor}
                        height={50}
                        width="40%"
                        onClick={() => this.props.navigation.navigate("EmployerJobListing")}
                    />
                </View>
                {props.thirdButton ?
                    <View style={{ alignItems: "center", width: "80%" }}>
                        <ButtonComponent
                            buttonName={props.thirdButton}
                            backgroundColor1={props.thirdButtonColor}
                            height={50}
                            width="100%"
                            onClick={() => this.props.navigation.navigate("EmployerJobListing")}
                        />
                    </View>
                    : null}
            </View>
        </View>
    );

}

export default ProfileEmpJob;

const styles = StyleSheet.create({

    cardText: {
        color: "#333333",
        fontFamily: FontFamily.Poppins_Light,
        //lineHeight: 19,
        textAlign: 'justify',
        fontSize: 14
    },
    firstbody: {
        flex: 0.36,
        //borderBottomWidth:0.2,
        borderColor: "grey",
        paddingHorizontal: 20,
        elevation: .55,
        paddingVertical: 15
    },
    secondbody: {
        backgroundColor: "#e9e9e9",
        marginTop: 5,
        marginBottom: 0,
        flex: 0.41,
        paddingHorizontal: 18,
        // borderBottomWidth:0.3,
        borderColor: "grey",
        paddingBottom: 18,
        elevation: .55
    },
    Headline: {
        fontSize: FontSize.medium_size,
        fontFamily: FontFamily.Poppins_Medium
    },
    firstcontainer: {
        flexDirection: "row",
        marginBottom: 7
    },
    aboutcompanytitle: {
        fontSize: FontSize.medium_size,
        fontFamily: FontFamily.Poppins_Medium
    },
    leftside: {
        flex: 0.35,
        justifyContent: "flex-start"
    },
    leftsidetext: {
        fontSize: 14,
        fontFamily: FontFamily.Poppins_Semibold, fontWeight: "600",
    },
    rightside: {
        flex: 0.62
    },
    textrightside: {
        fontSize: 14,
        fontFamily: FontFamily.Poppins_Semibold, fontWeight: "600",
        color: Colors.lightgray
    },
    meddleside: {
        flex: 0.08,
        justifyContent: "flex-start"
    },
    ReadMore: {
        color: "#000000"
    },
})