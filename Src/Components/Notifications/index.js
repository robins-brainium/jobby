import React, { Component } from 'react';
import { View, Text, StyleSheet, } from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import Colors from '../../Utils/Colors';
import FontSize from "./../../Utils/fonts";
import FontFamily from '../../Utils/FontFamily';

const NotificationComponent = (props) => {
  return (
    <View style={styles.container}>
      <View style={styles.firstcontainer}>
        <Text style={styles.question}>{props.Question} ?</Text>
        <Entypo
          name='dots-three-vertical'
          size={20}
          color="#767676"
          style={{}} />
      </View>
      <View style={{ marginHorizontal: 5 }}>
        <Text style={styles.text}>{props.Description}</Text>
        <Text style={styles.text}>{props.Date}</Text>
      </View>

    </View >
  );
}


export default NotificationComponent;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.WhiteColor,
    padding: 15,
    shadowOpacity: 0.1,
    shadowRadius: 5,
    elevation: 3,
    marginBottom: 5,
    marginTop: 5
  },
  firstcontainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: 5
  },
  question: {
    fontWeight: '600',
    fontFamily: FontFamily.Poppins_Medium,
    fontSize: FontSize.medium_size,
    color: "#000",
    marginBottom: 5
  },
  text: {
    fontWeight: '400',
    fontFamily: FontFamily.Poppins_Regular,
    fontSize: 15,
    color: "#a1a1a1",
    marginBottom: 5
  }
})