import React, { Component } from 'react';
import { View, Text, StyleSheet,TouchableOpacity } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import EvilIcons from 'react-native-vector-icons/EvilIcons';

const EmployerComponent = (props) => {
  return (
    <View style={styles.container}>
      <View style={{ backgroundColor: "#ffffff", }}>
        <Text style={styles.CompanyName}>{props.CompanyName}</Text>
        <View style={{flexDirection: "row"}}>
            <View style={{marginTop:2}}>
                <EvilIcons
                  style={{ marginBottom: 10, }}
                  name="location"
                  color="gray"
                  size={25}
                />
            </View>
                <Text style={styles.Location}>{props.Location}</Text>
        </View>
        
      </View >
      <TouchableOpacity style={{ alignItems: "flex-start", marginTop:15,}}>
              <MaterialCommunityIcons
                name="dots-vertical"
                size={22}
                color="#a7a7a7"
              />
             
          </TouchableOpacity>
    </View>
  );
}


export default EmployerComponent;

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#e9e9e9",
    marginBottom: 2, 
    justifyContent: "space-between",
    flexDirection: "row",
    padding: 18,
    elevation:1
  },
    CompanyName: {
    fontWeight: '600',
    fontFamily: 'Poppins-Medium',
    fontSize: 16,
    color: "#333333"
  },
  Location: {
    fontWeight: '400',
    fontFamily: 'Poppins-Medium',
    fontSize: 14,
    color: "#4a4a4a",
    marginLeft:10
  }
})