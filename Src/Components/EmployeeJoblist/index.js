import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import FontSize from '../../Utils/fonts';
import FontFamily from '../../Utils/FontFamily';
import Feather from 'react-native-vector-icons/Feather';


const Employee_JobList = (props) => {
  const { show } = props;
  const absoluteview = () => {
    return (
      <View style={{ position: "absolute", backgroundColor: "#fff", paddingHorizontal: 20, paddingVertical: 10, zIndex: 99999999, top: 20, right: 0, elevation: 5, minWidth: 150 }}>
        <TouchableOpacity style={{ paddingVertical: 5, borderBottomColor: "#e0e0e0", borderBottomWidth: 1 }} activeOpacity={0.5} onPress={() => props.apply()}>
          <Text style={{ fontFamily: FontFamily.Poppins_Medium, color: "#666" }}>Apply</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ paddingVertical: 5, borderBottomColor: "#e0e0e0", borderBottomWidth: 1 }} onPress={() => props.savenow()}>
          <Text style={{ fontFamily: FontFamily.Poppins_Medium, color: "#666" }}>Save Now</Text>
        </TouchableOpacity>
      </View>
    )
  }

  const savejobview = () => {
    return (
      <View style={{ position: "absolute", backgroundColor: "#fff", paddingHorizontal: 20, paddingVertical: 10, zIndex: 99999999, top: 20, right: 0, elevation: 5, minWidth: 150 }}>
        <TouchableOpacity style={{ paddingVertical: 5, borderBottomColor: "#e0e0e0", borderBottomWidth: 1 }} activeOpacity={0.5} onPress={() => props.apply()}>
          <Text style={{ fontFamily: FontFamily.Poppins_Medium, color: "#666" }}>Apply</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{ paddingVertical: 5, borderBottomColor: "#e0e0e0", borderBottomWidth: 1 }} // onPress={() => props.savenow()}
        activeOpacity={0.5}
        >
          <Text style={{ fontFamily: FontFamily.Poppins_Medium, color: "#666" }}>Save Discard</Text>
        </TouchableOpacity>
      </View>
    )
  }



  return (
    <View style={styles.maincontainer}>
      <View style={styles.secondcontainer} >
        <View style={{ width: "30%" }}>
          <Text style={styles.title}>{props.title}</Text>
          <Image style={{ width: 75, height: 30, alignSelf: "center", flex:.33 }} resizeMode="contain" source={require('../../Images/EmployeeJobList/logo.png')} />
        </View>
        <TouchableOpacity style={styles.view1} onPress={props.onClick()} activeOpacity={1}>
          <Text style={styles.CompanyName}>{props.CompanyName}</Text>
          <View style={{ flex: 1, }}>
            <View style={styles.repeatcontainer}>
              <View style={styles.yearimage}>
                <SimpleLineIcons
                  style={{ marginBottom: 10, marginLeft: 3 }}
                  name="briefcase"
                  color="gray"
                  size={15}
                />
              </View>
              <Text style={styles.text}>{props.years}</Text>
            </View>
            <View style={styles.repeatcontainer}>
              <View style={styles.yearimage}>
                <SimpleLineIcons
                  style={{ marginBottom: 0, }}
                  name="location-pin"
                  color="gray"
                  size={18}
                />
              </View>
              <Text style={styles.text}>{props.locationname}</Text>
            </View>
            <View style={styles.repeatcontainer}>
              <View style={styles.postimage}>
              <Feather
                  style={{ marginBottom: 0, }}
                  name="pen-tool"
                  color="gray"
                  size={18}
                />
              </View>
              <Text style={[styles.text]}>{props.desingnation}</Text>
            </View>
            <View style={{ paddingLeft: "12%" }}>
              <Text style={[styles.text2, { marginLeft: 20 }]}>{props.posteddate}</Text>
            </View>
          </View>
          {show ?
            absoluteview()
            : null
          }
          {props.savejoblist ?
            savejobview()
            : null
          }
        </TouchableOpacity>

        <View style={styles.view2}>
          <TouchableOpacity onPress={() => props.gotodetails()}>
            <Entypo
              style={{ marginBottom: 15 }}
              name="dots-three-vertical"
              color="gray"
              size={20}
            />
          </TouchableOpacity>
          <TouchableOpacity>
            <AntDesign
              style={{ marginBottom: 10 }}
              name="delete"
              color="gray"
              size={20}
            />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}


export default Employee_JobList;

const styles = StyleSheet.create({
  maincontainer: {
    backgroundColor: "#e9e9e9",
    marginBottom: 20,
    //elevation: 3
    shadowOpacity: 0.3,
    shadowOffset: 5,
    // elevation: 5,
    shadowOffset: {
    width: 0,
    height: 2
    },
  },
  secondcontainer: {
    backgroundColor: "#ffffff",
    padding: 15,
    flexDirection: "row",
    flex: 1
  },
  view1: {
    flex: 0.9,
  },
  view2: {
    flex: 0.1,
    alignItems: "center",
    justifyContent: "flex-start",
    marginTop: 20
  },
  title: {
    fontSize: FontSize.medium_size,
    fontFamily: FontFamily.Poppins_Medium,
    fontWeight: "600"
  },
  CompanyName: {
    marginBottom: 10,
    paddingHorizontal: 8,
    fontSize: FontSize.micro_size,
    fontFamily: FontFamily.Poppins_Medium,
    //fontWeight: "600",
    color: "#707070",
  },
  repeatcontainer: {
    flexDirection: "row", alignItems: "flex-start", flex: 1, paddingLeft:10
  },
  yearimage: {
    // alignItems: "center",

  },
  postimage: {
    //flex: 0.20,
    alignItems: "center",
    // marginLeft: 10
    //borderWidth:1
  },
  text: {
    marginBottom: 8, paddingLeft: "8%", fontFamily: FontFamily.Poppins_Medium, color: "#494949"
  },
  text2: {
    fontFamily: FontFamily.Poppins_Medium, color: "#707070", fontSize: FontSize.micro_size,
    fontWeight: '600'
  },
})