import React, { useEffect, useState, useReducer } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, TextInput, ScrollView, Picker, Platform, Dimensions } from 'react-native';
import FontFamily from '../../Utils/FontFamily';
import { RadioButton } from 'react-native-paper';
import ButtonComponent from '../Button/loginButton';
import FontSize from '../../Utils/fonts';
import HeaderComponent from '../Header';
import Colors from '../../Utils/Colors';
import RNPickerSelect from 'react-native-picker-select';
import FloatingLabelInput from '../FlotingTextInput';

const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;

const PreferredJob = (props) => {
  const [company, setcompany] = useState('');
  const [jobrole, setjobrole] = useState('');
  const [joblabel, setjoblabel] = useState('');
  const [joblocation, setlocation] = useState('');
  const [description, setdescription] = useState('');
  const [industry, setindustry] = useState('');
  const [employment, setemployment] = useState('');
  const [mounthsalary, setmonthalysalary] = useState('');
  const [notice, setnotice] = useState('');


  const saveAndContinue = (e) => {
    e.preventDefault()
    props.nextStep()
  }
  const back = (e) => {
    e.preventDefault();
    props.prevStep();
  }

  const renderRadioCircleIos = (radio) => {
    Platform.OS === 'ios' 
    return( <View style={[{left: radio}, styles.radioCircle]}/> )
  }

  return (
    <View style={{flex:1}}>
      <ScrollView style={{ flex: 1 }}>
        <Text style={{ textAlign: "center", paddingVertical: "5%", fontSize: FontSize.medium_size, fontFamily: FontFamily.Poppins_Medium }}>Enter Preferred Job</Text>
        <View style={styles.repeatContainer}>
          <FloatingLabelInput
            textInputHeight={30}
            textInputWidth={screenwidth / 1.25}
            label="Job Title"
            value={company}
            onChangeText={(val) => setcompany(val)}
            placeholder="Ex. Digital Marketing Manager"
          />
          {/* <Text style={styles.title}>Job Title</Text>
          <TextInput
            underlineColorAndroid="transparent"
            placeholder="Job title"
            placeholderTextColor="#999"
            autoCapitalize="none"
            onChangeText={(val) => setcompany(val)}
            value={company}
            style={styles.mytextinput}
          /> */}
        </View>
        <View style={[styles.repeatContainer]}>
          <Text style={styles.title}>Preferred Job Role</Text>
          <View style={{ borderBottomColor: "#999", borderBottomWidth: 1, width: "100%", height: 40, justifyContent: "center", }}>
          <RNPickerSelect
            onValueChange={(value) => setjobrole(value)}
            placeholder={{
              label: 'Ex. React Native Developer',
              value: null,
            }}
            placeholderTextColor="#999"
            style={pickerSelectStyles}
            items={[
                { label: 'React Native Developer', value: 'rc_dev' },
                { label: 'Web Developer', value: 'wb_dev' },
                { label: '.Net Developer', value: 'railway' },
            ]}
            />
          </View>
        </View>
          

        <View style={[styles.repeatContainer]}>
          <Text style={styles.title}>Job Level</Text>
          <View style={{ borderBottomColor: "#999", borderBottomWidth: 1, width: "100%", height: 40, justifyContent: "center", }}>
          <RNPickerSelect
            onValueChange={(value) => setjoblabel(value)}
            placeholder={{
              label: 'Select Job Level...',
              value: null,
            }}
            placeholderTextColor="#999"
            style={pickerSelectStyles}
            items={[
                { label: 'Executive', value: 'executive' },
                { label: 'Manager', value: 'manager' },
                { label: 'Supervisor', value: 'supervisor' },
                { label: 'officer', value: 'officer' },
                { label: 'etc', value: 'etc' },
            ]}
            />
          </View>
        </View>
        {/* <View style={styles.repeatContainer}>
          <Text style={styles.title}>Job Level</Text>
          <View style={{ flexDirection: "row", flexWrap: "wrap", paddingStart:5 }}>
            <RadioButton.Group
              onValueChange={value => setjoblabel(value)}
              value={joblabel}>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>Executive</Text>
                {renderRadioCircleIos('68%')}
                <RadioButton value="executive" />
              </View>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>Manager</Text>
                {renderRadioCircleIos('68%')}
                <RadioButton value="manager" />
              </View>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>Supervisor</Text>
                {renderRadioCircleIos('71%')}
                <RadioButton value="supervisor" />
              </View>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>Officer</Text>
                {renderRadioCircleIos('61%')}
                <RadioButton value="officer" />
              </View>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>Etc</Text>
                {renderRadioCircleIos('45%')}
                <RadioButton value="etc" />
              </View>
            </RadioButton.Group>
          </View>
        </View> */}

        <View style={styles.repeatContainer}>
          {/* <Text style={styles.title}>Job Location</Text> */}
          <FloatingLabelInput
            textInputHeight={30}
            textInputWidth={screenwidth / 1.25}
            label="Location"
            value={joblocation}
            onChangeText={(val) => setlocation(val)}
            placeholder="Location"
          />
          {/* <TextInput
            underlineColorAndroid="transparent"
            placeholder="Location"
            placeholderTextColor="#999"
            autoCapitalize="none"
            onChangeText={(val) => setlocation(val)}
            value={joblocation}
            style={styles.mytextinput}
          /> */}
        </View>

        <View style={[styles.repeatContainer]}>
          <FloatingLabelInput
            textInputHeight={30}
            textInputWidth={screenwidth / 1.25}
            label="Personal Summary"
            value={description}
            onChangeText={(val) => setdescription(val)}
            placeholder="Enter Personal Summary"
          />
          {/* <Text style={styles.title}>Personal Summary</Text>
          <TextInput
            underlineColorAndroid="transparent"
            placeholder="Personal Summary"
            placeholderTextColor="#999"
            autoCapitalize="none"
            onChangeText={(val) => setdescription(val)}
            
            value={description}
            style={styles.mytextinput}
          /> */}
        </View>

        <View style={styles.repeatContainer}>
          <Text style={styles.title}>Preferred Industry</Text>
          <View style={{ borderBottomColor: "#999", borderBottomWidth: 1, width: "100%", height: 45, justifyContent: "center", backgroundColor: "#fff" }}>
             <RNPickerSelect
              placeholder={{
                label: 'Ex. Information Technology',
                value: null,
              }}
              placeholderTextColor="#999"
              onValueChange={(value) => setindustry(value)}
              style={pickerSelectStyles}
              items={[
                  { label: 'Information Technology', value: 'it_job' },
                  { label: 'Goverment Job', value: 'gov_job' },
                  { label: 'Railway Job', value: 'railway' },
                  { label: 'BPO', value: 'bpo' },
              ]}
            />
            {/* <Picker
              selectedValue={industry}
              onValueChange={(itemValue, itemPosition) =>
                setindustry(itemValue)}>
              <Picker.Item label="Select Preferred Industry" value="" />
              <Picker.Item label="Information Technology" value="it_job" />
              <Picker.Item label="Goverment Job" value="gov_job" />
              <Picker.Item label="Railway Job" value="railway" />
              <Picker.Item label="BPO" value="bpo" />
            </Picker> */}
          </View>
        </View>

        <View style={styles.repeatContainer}>
          <Text style={styles.title}>Employment Type</Text>
          <View style={{ flexDirection: "row", flexWrap: "wrap"}}>
            <RadioButton.Group
              onValueChange={value => setemployment(value)}
              value={employment}>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>Fulltime</Text>
                {renderRadioCircleIos('65%')}
                <RadioButton value="fulltime" />
              </View>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>Part-time</Text>
                {renderRadioCircleIos('68%')}
                <RadioButton value="parttime" />
              </View>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>Temporary</Text>
                {renderRadioCircleIos('71%')}
                <RadioButton value="temporary" />
              </View>
            </RadioButton.Group>
          </View>
        </View>

        <View style={[styles.repeatContainer]}>
          <FloatingLabelInput
            textInputHeight={30}
            textInputWidth={screenwidth / 1.25}
            label="Terget Monthly"
            value={mounthsalary}
            onChangeText={(val) => setmonthalysalary(val)}
            placeholder="Enter Terget Monthly"
          />
          {/* <Text style={styles.title}>Terget Monthly</Text>
          <TextInput
            underlineColorAndroid="transparent"
            placeholder="Terget Monthly"
            placeholderTextColor="#999"
            autoCapitalize="none"
            onChangeText={(val) => setmonthalysalary(val)}
            value={mounthsalary}
            style={styles.mytextinput}
          /> */}
        </View>

        <View style={[styles.repeatContainer]}>
          <FloatingLabelInput
            textInputHeight={30}
            textInputWidth={screenwidth / 1.25}
            label="Noice Period"
            value={notice}
            onChangeText={(val) => setnotice(val)}
            placeholder="Enter Notice Period(Only Numbers)"
          />
          {/* <Text style={styles.title}>Notice Period</Text>
          <TextInput
            underlineColorAndroid="transparent"
            placeholder="Noice Period"
            placeholderTextColor="#999"
            autoCapitalize="none"
            onChangeText={(val) => setnotice(val)}
            value={notice}
            style={styles.mytextinput}
          /> */}
        </View>

        {/* <View style={{ flexDirection: "row", alignSelf: "center", width: "80%", justifyContent: "space-between", marginBottom: 20 }}>
          <ButtonComponent
            buttonName="Back"
            backgroundColor1="#1a2246"
            height={40}
            width="45%"
            onClick={back}
          />
          <ButtonComponent
            buttonName="Save"
            backgroundColor1="#1a2246"
            height={40}
            width="45%"
            onClick={saveAndContinue}
          />
        </View> */}
      </ScrollView>
    </View>
  );
}

export default PreferredJob;

const styles = StyleSheet.create({
  repeatContainer: {
    width: "80%", alignItems: "flex-start", alignSelf: "center", marginBottom: 15
  },
  mytextinput: {
    backgroundColor: "#fff", borderColor: "#999", borderWidth: 1, borderRadius: 10, height: 35, width: "100%", paddingHorizontal: 10, fontSize:FontSize.small_size, justifyContent:"center"
  },
  radioCircle: {
    padding: 9, 
    borderWidth: 2, 
    borderColor: Colors.Nero, 
    borderRadius: 100, 
    position: 'absolute'
  },
  title:{
    fontSize:14,
    fontFamily:FontFamily.Poppins_Medium
    //paddingStart:5
  }
})

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 15,
    paddingVertical: 10,
    paddingHorizontal: 0,
    // borderWidth: 1,
    // borderColor: 'gray',
    // borderRadius: 4,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 15,
    paddingHorizontal: 0,
    paddingVertical: 10,
    borderWidth: 0.5,
    borderColor: 'purple',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
});