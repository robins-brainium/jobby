import React, { useEffect, useState, useReducer } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, TextInput, ScrollView, Picker, Platform, Dimensions, Switch } from 'react-native';
import FontFamily from '../../Utils/FontFamily';
import { RadioButton } from 'react-native-paper';
import ButtonComponent from '../Button/loginButton';
import FontSize from '../../Utils/fonts';
import HeaderComponent from '../Header';
import Colors from '../../Utils/Colors';
import RNPickerSelect from 'react-native-picker-select';
import FloatingLabelInput from '../FlotingTextInput';



const Experience = (props) => {
  const [title, settitle] = useState('');
  const [companyname, setcompanyname] = useState('');
  const [jobrole, setjobrole] = useState('');
  const [description, setdescription] = useState('');
  const [country, setcountry] = useState('');
  const [city, setcity] = useState('');
  const [startdate, setStartdate] = useState('');
  const [enddate, setEnddate] = useState('');
  const [presentwork, setpresentwork] = useState('');
  const [switch1Value, setswitch] = useState(false);
  

  
  const screenHeight = Dimensions.get('window').height;
  const screenwidth = Dimensions.get('window').width;

  const saveAndContinue = (e) => {
    e.preventDefault()
    props.nextStep()
  }
  const back = (e) => {
    e.preventDefault();
    props.prevStep();
  }

  const renderRadioCircleIos = (radio) => {
    Platform.OS === 'ios' 
    return( <View style={[{left: radio}, styles.radioCircle]}/> )
  }

  const toggleSwitch1 = (value) => {
    setswitch(value)
    // this.setState({switch1Value: value})
    console.log('Switch 1 is: ' + value)
  }

  return (
    <View style={{flex:1}}>
      <ScrollView style={{ flex: 1 }}>
        <Text style={{ textAlign: "center", paddingVertical: "5%", fontSize: FontSize.medium_size, fontFamily: FontFamily.Poppins_Medium }}>Enter Job Experience</Text>
        <View style={styles.repeatContainer}>
          <FloatingLabelInput
            textInputHeight={30}
            textInputWidth={screenwidth / 1.25}
            label="Job Title"
            value={title}
            onChangeText={(val) => settitle(val)}
            placeholder="Ex. Project Manager"
          />
          {/* <Text style={styles.title}>Job Title</Text>
          <TextInput
            underlineColorAndroid="transparent"
            placeholder="Job title"
            placeholderTextColor="#999"
            autoCapitalize="none"
            onChangeText={(val) => settitle(val)}
            value={title}
            style={styles.mytextinput}
          /> */}
        </View>
        <View style={[styles.repeatContainer]}>
          <Text style={styles.title}>Field</Text>
          <View style={{ borderBottomColor: "#999", borderBottomWidth: 1, width: "100%", height: 40, justifyContent: "center", backgroundColor: "#fff" }}>
          <RNPickerSelect
             placeholder={{
              label: 'Select Job Role...',
              value: null,
            }}
            placeholderTextColor="#999"
            onValueChange={(value) => setjobrole(value)}
            style={pickerSelectStyles}
            items={[
                { label: 'React Native Developer', value: 'rc_dev' },
                { label: 'Web Developer', value: 'wb_dev' },
                { label: '.Net Developer', value: 'railway' },
                { label: 'Web Designer', value: 'wb_designer' },
            ]}
            />
            {/* <Picker
              selectedValue={jobrole}
              onValueChange={(itemValue, itemPosition) =>
                setjobrole(itemValue)}>
              <Picker.Item label="Field" value="" />
              <Picker.Item label="React Native Developer" value="rc_dev" />
              <Picker.Item label="Web Developer" value="wb_dev" />
              <Picker.Item label=".Net Developer" value="dotnet" />
              <Picker.Item label="Web Designer" value="wb_designer" />
            </Picker> */}
          </View>
        </View>


        <View style={styles.repeatContainer}>
          <FloatingLabelInput
            textInputHeight={30}
            textInputWidth={screenwidth / 1.25}
            label="Company Name"
            value={companyname}
            onChangeText={(val) => setcompanyname(val)}
            placeholder="Ex. Brainium Information Technology"
          />
          {/* <Text style={styles.title}>Company Name</Text>
          <TextInput
            underlineColorAndroid="transparent"
            placeholder="Location"
            placeholderTextColor="#999"
            autoCapitalize="none"
            onChangeText={(val) => setcompanyname(val)}
            value={companyname}
            style={styles.mytextinput}
          /> */}
        </View>

        <View style={[styles.repeatContainer]}>
          <FloatingLabelInput
            textInputHeight={30}
            textInputWidth={screenwidth / 1.25}
            label="Job Location (Country)"
            value={country}
            onChangeText={(val) => setcountry(val)}
            placeholder="Enter Job Location"
          />
          {/* <Text style={styles.title}>Job Location (Country)</Text> */}
          {/* <TextInput
            underlineColorAndroid="transparent"
            placeholder="Country"
            placeholderTextColor="#999"
            autoCapitalize="none"
            onChangeText={(val) => setcountry(val)}
            value={country}
            style={styles.mytextinput}
          /> */}
        </View>

        <View style={[styles.repeatContainer]}>
          <FloatingLabelInput
            textInputHeight={30}
            textInputWidth={screenwidth / 1.25}
            label="Job Location (City)"
            value={country}
            onChangeText={(val) => setcity(val)}
            placeholder="Enter City Name"
          />
          {/* <Text style={styles.title}>Job Location (City)</Text>
          <TextInput
            underlineColorAndroid="transparent"
            placeholder="City"
            placeholderTextColor="#999"
            autoCapitalize="none"
            onChangeText={(val) => setcity(val)}
            value={city}
            style={styles.mytextinput}
          /> */}
        </View>

        <View style={[styles.repeatContainer]}>
          {/* <Text style={styles.title}>Enter Start Date &amp; End Date</Text> */}
          <View style={{ flexDirection: "row", justifyContent: "space-between", width: "100%" }}>
            
            <FloatingLabelInput
              textInputHeight={30}
              textInputWidth={screenwidth / 2.60}
              label="Start Date"
              value={startdate}
              onChangeText={(val) => setStartdate(val)}
              placeholder="Enter Start Date"
            />
              {/* <TextInput
                underlineColorAndroid="transparent"
                placeholder="Start Date"
                placeholderTextColor="#999"
                autoCapitalize="none"
                onChangeText={(val) => setStartdate(val)}
                value={startdate}
                style={[styles.mytextinput]}
              /> */}
              { switch1Value == false ?
                <FloatingLabelInput
                textInputHeight={30}
                textInputWidth={screenwidth / 2.60}
                label="End Date"
                value={enddate}
                onChangeText={(val) => setEnddate(val)}
                placeholder="Enter End Date"
              />
              :null
              }
                          
              {/* <TextInput
                underlineColorAndroid="transparent"
                placeholder="End Date"
                placeholderTextColor="#999"
                autoCapitalize="none"
                onChangeText={(val) => setEnddate(val)}
                value={enddate}
                style={[styles.mytextinput]}
              /> */}
          </View>
        </View>

        <View style={styles.repeatContainer}>
          <View style={{ flexDirection: "row", flexWrap: "wrap", alignItems:"center" }}>
          <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular, marginRight:10 }}>I still work here</Text>
            <Switch
              onValueChange = {toggleSwitch1}
              value = {switch1Value}/>
            {/* <RadioButton.Group
              onValueChange={value => setpresentwork(value)}
              value={presentwork}>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>I still work here</Text>
                {renderRadioCircleIos('76%')}
                <RadioButton value="present_work" />
              </View>
            </RadioButton.Group> */}
          </View>
        </View>


        <View style={[styles.repeatContainer]}>
          <Text style={styles.title}>Description</Text>
          {/* <FloatingLabelInput
            textInputHeight={80}
            textInputWidth={screenwidth / 1.25}
            label="Description"
            value={description}
            onChangeText={(val) => setdescription(val)}
          /> */}
           <TextInput
            underlineColorAndroid="transparent"
            placeholder="Enter your job project name, work environment, office cultute and other things."
            placeholderTextColor="#999"
            autoCapitalize="none"
            onChangeText={(val) => setdescription(val)}
            multiline={true}
            value={description}
            style={[styles.mytextinput, { height: 80 }]}
          /> 
        </View>

        {/* <View style={{ flexDirection: "row", alignSelf: "center", width: "80%", justifyContent: "space-between", marginBottom: 20 }}>
          <ButtonComponent
            buttonName="Back"
            backgroundColor1="#1a2246"
            height={40}
            width="45%"
            onClick={back}
          />
          <ButtonComponent
            buttonName="Save"
            backgroundColor1="#1a2246"
            height={40}
            width="45%"
            onClick={saveAndContinue}
          />
        </View> */}
      </ScrollView>
    </View>
  );
}


export default Experience;

const styles = StyleSheet.create({
  repeatContainer: {
    width: "80%", alignItems: "flex-start", alignSelf: "center", marginBottom: 15
  },
  mytextinput: {
    backgroundColor: "#fff", borderColor: "#999", borderWidth: 1, height: 40, width: "100%", paddingHorizontal: 10, textAlignVertical: 'top'
  },
  radioCircle: {
    padding: 9, 
    borderWidth: 2, 
    borderColor: Colors.Nero, 
    borderRadius: 100, 
    position: 'absolute'
  },
  title:{
    fontSize:12,
     marginBottom:5,
     paddingStart:2
  }
})

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 15,
    paddingVertical: 0,
    paddingHorizontal: 0,
    // borderWidth: 1,
    // borderColor: 'gray',
    // borderRadius: 4,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 15,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: 'purple',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
});