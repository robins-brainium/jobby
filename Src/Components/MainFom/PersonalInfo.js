import React, { useEffect, useState, useReducer } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, TextInput, ScrollView, Platform, Dimensions } from 'react-native';
import FontFamily from '../../Utils/FontFamily';
import ButtonComponent from '../Button/loginButton';
import { RadioButton } from 'react-native-paper';
import FontSize from '../../Utils/fonts';
import HeaderComponent from '../Header';
import Colors from '../../Utils/Colors';
import FloatingLabelInput from '../../Components/FlotingTextInput';


const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;

const PersonalInfo = (props) => {
  const [company, setcompany] = useState('');
  const [country, setcountry] = useState('');
  const [city, setcity] = useState('');
  const [dob, setdob] = useState('');
  const [nationality, setnationality] = useState('');
  const [rcountry, setrcountry] = useState('');
  const [visa, setvisa] = useState('');
  const [material, setmaterial] = useState('');
  const [driving, setdriving] = useState('');

  const saveAndContinue = (e) => {
    e.preventDefault()
    props.nextStep()
  }

  const renderRadioCircleIos = (radio) => {
    Platform.OS === 'ios' 
    return( <View style={[{left: radio}, styles.radioCircle]}/> )
  }

  return (
    <View style={{flex:1}}>
      <ScrollView style={{ flex: 1 }}>
        <Text style={{ textAlign: "center", paddingVertical: "5%", fontSize: FontSize.medium_size, fontFamily: FontFamily.Poppins_Medium }}>Enter Your Profile Information</Text>
        <View style={styles.repeatContainer}>
          {/* <Text style={styles.title}>Add Current Company Name</Text> */}
          <FloatingLabelInput
            textInputHeight={30}
            textInputWidth={screenwidth / 1.25}
            label="Company Name"
            value={company}
            onChangeText={(val) => setcompany(val)}
            placeholder="Enter Company Name"
          />
          {/* <TextInput
            underlineColorAndroid="transparent"
            placeholder="Company Name"
            placeholderTextColor="#999"
            autoCapitalize="none"
            onChangeText={(val) => setcompany(val)}
            value={company}
            style={styles.mytextinput}
          /> */}
        </View>
        <View style={styles.repeatContainer}>
          {/* <Text style={styles.title}>Country</Text> */}
          <FloatingLabelInput
            textInputHeight={30}
            textInputWidth={screenwidth / 1.25}
            label="Country Name"
            value={country}
            onChangeText={(val) => setcountry(val)}
            placeholder="Enter Country Name"
          />
          {/* <TextInput
            underlineColorAndroid="transparent"
            placeholder="Entry Country Name"
            placeholderTextColor="#999"
            autoCapitalize="none"
            onChangeText={(val) => setcountry(val)}
            value={country}
            style={styles.mytextinput}
          /> */}
        </View>
        <View style={styles.repeatContainer}>
          {/* <Text style={styles.title}>City</Text> */}
          <FloatingLabelInput
            textInputHeight={30}
            textInputWidth={screenwidth / 1.25}
            label="City Name"
            value={city}
            onChangeText={(val) => setcity(val)}
            placeholder="Enter City Name"
          />
          {/* <TextInput
            underlineColorAndroid="transparent"
            placeholder="Entry City Name"
            placeholderTextColor="#999"
            autoCapitalize="none"
            onChangeText={(val) => setcity(val)}
            
            value={city}
            style={styles.mytextinput}
          /> */}
        </View>
        <View style={styles.repeatContainer}>
          {/* <Text style={styles.title}>Date Of Birth</Text> */}
          <FloatingLabelInput
            textInputHeight={30}
            textInputWidth={screenwidth / 1.25}
            label="Date of Birth"
            value={dob}
            onChangeText={(val) => setdob(val)}
            placeholder="Your Date of birth"
          />
          {/* <TextInput
            underlineColorAndroid="transparent"
            placeholder="Date of Birth"
            placeholderTextColor="#999"
            autoCapitalize="none"
            onChangeText={(val) => setdob(val)}
            
            value={dob}
            style={styles.mytextinput}
          /> */}
        </View>

        <View style={styles.repeatContainer}>
          {/* <Text style={styles.title}>Nationality</Text> */}
          <FloatingLabelInput
            textInputHeight={30}
            textInputWidth={screenwidth / 1.25}
            label="Nationality"
            value={nationality}
            onChangeText={(val) => setnationality(val)}
            placeholder="Enter Your Nationality"
          />
          {/* <TextInput
            underlineColorAndroid="transparent"
            placeholder="Nationality"
            placeholderTextColor="#999"
            autoCapitalize="none"
            onChangeText={(val) => setnationality(val)}
            
            value={nationality}
            style={styles.mytextinput}
          /> */}
        </View>

        <View style={styles.repeatContainer}>
          <FloatingLabelInput
            textInputHeight={30}
            textInputWidth={screenwidth / 1.25}
            label="Residence Country"
            value={rcountry}
            onChangeText={(val) => setrcountry(val)}
            placeholder="Enter Residence Country"
          />
          {/* <Text style={styles.title}>Residence Country</Text>
          <TextInput
            underlineColorAndroid="transparent"
            placeholder="Residence Country"
            placeholderTextColor="#999"
            autoCapitalize="none"
            onChangeText={(val) => setrcountry(val)}
            
            value={rcountry}
            style={styles.mytextinput}
          /> */}
        </View>
        
        <View style={[styles.repeatContainer]}>
          <Text style={styles.title}>Upload Image</Text>
          <ButtonComponent
            buttonName="Upload image"
            backgroundColor1="#1a2246"
            height={40}
            width="50%"
          // onClick={() => this.props.navigation.navigate("PostJob")}
          />
        </View>
        <View style={styles.repeatContainer}>
          <Text style={styles.title}>Visa Status</Text>
          <View style={{ flexDirection: "row", flexWrap: "wrap", paddingStart:5 }}>
            <RadioButton.Group
              onValueChange={value => setvisa(value)}
              value={visa}>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>Yes</Text>
                {renderRadioCircleIos('50%')}
                <RadioButton value="yes" />
              </View>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>No</Text>
                {renderRadioCircleIos('45%')}
                <RadioButton value="no" />
              </View>
            </RadioButton.Group>
          </View>
        </View>

        <View style={styles.repeatContainer}>
          <Text style={styles.title}>Material Status</Text>
          <View style={{ flexDirection: "row", flexWrap: "wrap", paddingStart:5 }}>
            <RadioButton.Group
              onValueChange={value => setmaterial(value)}
              value={material}>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>Yes</Text>
                {renderRadioCircleIos('50%')}
                <RadioButton value="yes" />
              </View>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>No</Text>
                {renderRadioCircleIos('45%')}
                <RadioButton value="no" />
              </View>
            </RadioButton.Group>
          </View>
        </View>

        <View style={styles.repeatContainer}>
          <Text style={styles.title}>Driving Licence</Text>
          <View style={{ flexDirection: "row", flexWrap: "wrap", paddingStart:5 }}>
            <RadioButton.Group
              onValueChange={value => setdriving(value)}
              value={driving}>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>Yes</Text>
                {renderRadioCircleIos('50%')}
                <RadioButton value="yes" />
              </View>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>No</Text>
                {renderRadioCircleIos('45%')}
                <RadioButton value="no" />
              </View>
            </RadioButton.Group>
          </View>
        </View>

        
      </ScrollView>
    </View>
  );
}


export default PersonalInfo;

const styles = StyleSheet.create({
  repeatContainer: {
    width: "80%", alignItems: "flex-start", alignSelf: "center", marginBottom: 20
  },
  mytextinput: {
    backgroundColor: "#fff", borderColor: "#999", borderWidth: 1, borderRadius: 10, height: 40, width: "100%", paddingHorizontal: 10, textAlignVertical: 'top'
  },

  radioCircle: {
    padding: 9, 
    borderWidth: 2, 
    borderColor: Colors.Nero, 
    borderRadius: 100, 
    position: 'absolute'
  },
  title:{
    marginBottom:10,
    paddingStart:5
  }
})