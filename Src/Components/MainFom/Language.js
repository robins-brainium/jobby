import React, { useEffect, useState, useReducer } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, TextInput, ScrollView, Picker, Platform } from 'react-native';
import FontFamily from '../../Utils/FontFamily';
import { RadioButton } from 'react-native-paper';
import ButtonComponent from '../Button/loginButton';
import FontSize from '../../Utils/fonts';
import HeaderComponent from '../Header';
import Colors from '../../Utils/Colors';
import RNPickerSelect from 'react-native-picker-select';


const Language = (props) => {
  const [lavel, setlavel] = useState('');
  const [language, setlanguage] = useState('');

  const saveAndContinue = (e) => {
    e.preventDefault()
    props.nextStep()
  }

  const back = (e) => {
    e.preventDefault();
    props.prevStep();
  }

  const renderRadioCircleIos = (radio) => {
    Platform.OS === 'ios' 
    return( <View style={[{left: radio}, styles.radioCircle]}/> )
  }

  return (
    <View style={{flex:1}}>
      <ScrollView style={{ flex: 1 }}>
        <Text style={{ textAlign: "center", paddingVertical: "5%", fontSize: FontSize.medium_size, fontFamily: FontFamily.Poppins_Medium }}>Enter Language</Text>
        <View style={[styles.repeatContainer]}>
          <Text style={styles.title}>Language</Text>
          <View style={{ borderBottomColor: "#999", borderBottomWidth: 1, width: "100%", height: 40, justifyContent: "center", backgroundColor: "#fff" }}>
          <RNPickerSelect
            placeholder={{
              label: 'Choose language...',
              value: null,
            }}
            placeholderTextColor="#999"
            onValueChange={(value) => setlanguage(value)}
            style={pickerSelectStyles}
            items={[
                { label: 'English', value: 'bsc' },
                { label: 'Arabic', value: 'bcom' },
                { label: 'Urdu', value: 'msc' },
            ]}
            />
            {/* <Picker
              selectedValue={language}
              onValueChange={(itemValue, itemPosition) =>
                setlanguage(itemValue)}>
              <Picker.Item label="Choose Langrage" value="" />
              <Picker.Item label="English" value="eng" />
              <Picker.Item label="Arbic" value="arabic" />
              <Picker.Item label="Urdu" value="urdu" />
            </Picker> */}
          </View>
        </View>
          
        <View style={[styles.repeatContainer]}>
          <Text style={styles.title}>Level</Text>
          <View style={{ borderBottomColor: "#999", borderBottomWidth: 1, width: "100%", height: 40, justifyContent: "center", backgroundColor: "#fff" }}>
          <RNPickerSelect
            placeholder={{
              label: 'Choose Lavel...',
              value: null,
            }}
            placeholderTextColor="#999"
            onValueChange={(value) => setlavel(value)}
            style={pickerSelectStyles}
            items={[
                { label: 'Beginner', value: 'beginner' },
                { label: 'Intermediate', value: 'intermediate' },
                { label: 'Expert', value: 'expert' },
                { label: 'Native', value: 'native' },
            ]}
            />
          </View>
        </View> 
        {/* <View style={styles.repeatContainer}>
          <Text style={styles.title}>Choose the level</Text>
          <View style={{ flexDirection: "row", flexWrap: "wrap", }}>
            <RadioButton.Group
              onValueChange={value => setlavel(value)}
              value={lavel}>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>Beginner</Text>
                {renderRadioCircleIos('67%')}
                <RadioButton value="beginner" />
              </View>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>Intermediate</Text>
                {renderRadioCircleIos('75%')}
                <RadioButton value="intermediate" />
              </View>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>Expert</Text>
                {renderRadioCircleIos('60%')}
                <RadioButton value="expert" />
              </View>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Text style={{ fontSize: FontSize.micro_size, fontFamily: FontFamily.Poppins_Regular }}>Native</Text>
                {renderRadioCircleIos('60%')}
                <RadioButton value="native" />
              </View>
            </RadioButton.Group>
          </View>
        </View> */}

        {/* <View style={{ flexDirection: "row", alignSelf: "center", width: "80%", justifyContent: "space-between", marginBottom: 20 }}>
          <ButtonComponent
            buttonName="Back"
            backgroundColor1="#1a2246"
            height={40}
            width="45%"
            onClick={back}
          />
          <ButtonComponent
            buttonName="Save"
            backgroundColor1="#1a2246"
            height={40}
            width="45%"
            onClick={saveAndContinue}
          />
        </View> */}

      </ScrollView>
    </View>
  );
}


export default Language;

const styles = StyleSheet.create({
  repeatContainer: {
    width: "80%", alignItems: "flex-start", alignSelf: "center", marginBottom: 10
  },
  mytextinput: {
    backgroundColor: "#fff", borderColor: "#999", borderWidth: 1, borderRadius: 10, height: 40, width: "100%", paddingHorizontal: 10, textAlignVertical: 'top'
  },
  radioCircle: {
    padding: 9, 
    borderWidth: 2, 
    borderColor: Colors.Nero, 
    borderRadius: 100, 
    position: 'absolute'
  },
  title:{
    fontSize:14,
    fontFamily:FontFamily.Poppins_Medium
    //marginBottom:5,
    // paddingStart:5
  }
})

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 15,
    paddingVertical: 12,
    paddingHorizontal: 0,
    // borderWidth: 1,
    // borderColor: 'gray',
    // borderRadius: 4,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 15,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: 'purple',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  title:{
    fontSize:12
    // marginBottom:5,
    // paddingStart:5
  }
});