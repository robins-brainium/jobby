import React, { useEffect, useState, useReducer } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, TextInput, ScrollView, Dimensions } from 'react-native';
import FontFamily from '../../Utils/FontFamily';
import { RadioButton } from 'react-native-paper';
import ButtonComponent from '../Button/loginButton';
import FontSize from '../../Utils/fonts';
import CountryPicker, { getAllCountries, getCallingCode } from 'react-native-country-picker-modal';
import HeaderComponent from '../Header';
import FloatingLabelInput from '../FlotingTextInput';

const screenHeight = Dimensions.get('window').height;
const screenwidth = Dimensions.get('window').width;

const ContactInfo = (props) => {
  console.log(props, "props");
  
  const [email, setemail] = useState('');
  const [phone, setphone] = useState('');
  const [country, setCountry] = useState(null)
  const [withFlag, setWithFlag] = useState(true)
  const [withCallingCode, setWithCallingCode] = useState(false)
  const [countryCode, setCountryCode] = useState('FR');

  

  const onSelect = (country) => {
    setCountryCode(country.cca2)
    setCountry(country)
  }

  const saveAndContinue = (e) => {
    e.preventDefault()
    props.nextStep()
  }

  const back = (e) => {
    e.preventDefault();
    props.navigation.goBack();
  }
  
  console.log(withCallingCode, "withCallingCode");

  return (
    <View style={{flex:1}}>
      <ScrollView style={{ flex: 1, }}>
        <Text style={{ textAlign: "center", paddingVertical: "5%", fontSize: FontSize.medium_size, fontFamily: FontFamily.Poppins_Medium }}>Enter Your Contact Information</Text>
        <View style={styles.repeatContainer}>
          <FloatingLabelInput
            textInputHeight={30}
            textInputWidth={screenwidth / 1.25}
            label="Enter Email"
            value={email}
            onChangeText={(val) => setemail(val)}
            placeholder="Entry Email Id"
          />
          {/* <TextInput
            underlineColorAndroid="transparent"
            placeholder="Entry Email Id"
            placeholderTextColor="#999"
            autoCapitalize="none"
            onChangeText={(val) => setemail(val)}
            value={email}
            style={styles.mytextinput}
          /> */}
        </View>

        <View style={styles.repeatContainer}>
          
          <View style={{ flexDirection: "row" }}>
            <View style={[styles.mytextinput, { width: 50, justifyContent: "center",  marginRight:10 }]}>
              <CountryPicker
                {...{
                  countryCode,
                  withFlag,
                  withCallingCode,
                  onSelect
                }}
              />
              {/* {country !== null && (
            <Text style={{fontSize:15}}>{JSON.stringify(country.callingCode[0], null, 2)}</Text>
          )} */}
            </View>
            {/* <FloatingLabelInput
              label="Enter "
              value={email}
              onChangeText={(val) => setphone(val)}
            /> */}
            <FloatingLabelInput
              textInputHeight={30}
              textInputWidth={screenwidth / 1.50}
              label="Entry Phone No"
              value={email}
              onChangeText={(val) => setphone(val)}
              placeholder="Entry Phone No"
            />
            {/* <TextInput
              underlineColorAndroid="transparent"
              placeholder="Entry Phone No"
              placeholderTextColor="#999"
              autoCapitalize="none"
              onChangeText={(val) => setphone(val)}
              value={phone}
              style={[styles.mytextinput, { width: "79%", marginLeft: 15 }]}
              maxLength={10}
              keyboardType={'numeric'}
            /> */}
          </View>
        </View>


        {/* <View style={{marginTop:"15%", flexDirection: "row", alignSelf: "center", width: "80%", justifyContent: "space-between" }}>
          <ButtonComponent
            buttonName="Back"
            backgroundColor1="#1a2246"
            height={45}
            width="45%"
            onClick={back}
          />
          <ButtonComponent
            buttonName="Save"
            backgroundColor1="#1a2246"
            height={45}
            width="45%"
            onClick={saveAndContinue}
          />
        </View> */}
        
      </ScrollView>
    </View>
  );
}


export default ContactInfo;

const styles = StyleSheet.create({
  repeatContainer: {
    width: "80%", alignItems: "flex-start", alignSelf: "center", marginBottom: 15
  },
  mytextinput: {
     borderBottomColor: "#999", borderBottomWidth: 1, height: 48, width: "100%", paddingHorizontal: 10, textAlignVertical: 'top'
  },
  title:{
    marginBottom:5,
    paddingStart:5
  }
})