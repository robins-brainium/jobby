import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Fontisto from 'react-native-vector-icons/Fontisto';
import Entypo from 'react-native-vector-icons/Entypo';
import FontSize from '../../Utils/fonts';
import FontFamily from '../../Utils/FontFamily';



const CompanyProfile = (props) => {
  return (
    <View style={styles.container}>
      <View style={styles.profileContainer}>
        <View style={styles.ImageContainer}>
          <Image source={props.image} style={styles.profileImage} />
        </View>
        <View style={styles.profileDescriptionContainer}>
          <Text style={styles.name}>{props.name}</Text>
          <Text style={styles.companyName}>{props.companyName}</Text>
          <Text style={styles.jobDescription}>{props.jobDescription}</Text>
        </View>
      </View>
      <TouchableOpacity style={{}}>
        <Fontisto
          name="email"
          size={30}
          color="gray"
        />
      </TouchableOpacity>
    </View>
  );
}


export default CompanyProfile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "space-between",
    flexDirection: "row",
    width: "93%",
    paddingLeft: 15,
    paddingBottom: 20,
    //paddingRight: 15,
    borderWidth:0,
    alignSelf: "center",
    borderColor:"#000",
    shadowOpacity: 0.1,
    elevation: 3, 
    shadowOffset: {
    width: 0,
    height: 4
    },
    marginBottom: 15,
    padding: 20
  },
  profileContainer: {
    flexDirection: "row",

  },
  ImageContainer: {
    height: 60,
    width: 60,
    borderRadius: 60,
    marginRight: 10
  },
  profileImage: {
    height: "100%",
    width: "100%",
    borderRadius: 60,
  },
  profileDescriptionContainer: {
    // justifyContent: "center"
    width: "73%",
    borderWidth:0
  },
  name: {
    fontFamily: FontFamily.Poppins_Medium,
    fontSize: FontSize.small_size,
    fontWeight: "700",
    marginBottom: 2,
    color:"#515A5A"
  },
  companyName: {
    fontFamily: FontFamily.Poppins_Semibold,
    fontSize: FontSize.small_size,
    fontWeight: "600",
    marginBottom: 15,
    color:"#515A5A"
  },
  jobDescription: {
    fontFamily: FontFamily.Poppins_Semibold,
    fontSize: FontSize.small_size,
    fontWeight: "600",
    marginBottom: 15,
    color: "#515A5A"
  },
})