import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native';
import FontSize from '../../Utils/fonts';
import FontFamily from '../../Utils/FontFamily';



const BlockReportUsers = (props) => {
  return (
    <View style={styles.container}>
      <View style={styles.profileContainer}>
        <View style={styles.ImageContainer}>
          <Image source={props.image} style={styles.profileImage} />
        </View>
        <View style={styles.profileDescriptionContainer}>
          <Text style={styles.name}>{props.name}</Text>
          <Text style={styles.description}>{props.description}</Text>
          <Text style={styles.unlock}>{props.unlock}</Text>
        </View>
      </View>

    </View>
  );
}


export default BlockReportUsers;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "space-between",
    flexDirection: "row",
    width: "90%",
    //paddingLeft: 30,
    paddingBottom: 30,
    // paddingRight: 200,
    borderWidth:0,
    alignSelf:"center"
  },
  profileContainer: {
    flexDirection: "row",

  },
  ImageContainer: {
    height: 65,
    width: 65,
    borderRadius: 65,
    marginRight: 15,
    marginLeft:10, 
    marginTop:5  
  },
  profileImage: {
    height: "100%",
    width: "100%",
    borderRadius: 65,
    borderColor: "#646464",
    borderWidth:.33
  },
  profileDescriptionContainer: {
    //justifyContent: "center",
    width:"74%", 
    borderWidth:0
  },
  name: {
    fontSize: 17,
    fontWeight: "600",
    fontFamily: FontFamily.Poppins_Medium,
    marginBottom: 3
  },
  description: {
    fontSize: 13,
    //fontWeight: "bold",
    fontFamily: FontFamily.Poppins_Light,
    color: "#757575",
    marginBottom: 5,
    borderWidth:0,
    justifyContent:"space-between"
  },
  unlock: {
    fontSize: 13,
    //fontWeight: "700",
    fontFamily: FontFamily.Poppins_Regular,
    color:"#ed1f24"
  },
})