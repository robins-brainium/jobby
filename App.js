import React, {useState, useEffect} from 'react'
import Navigator from './Src/Navigation/AppNavigator'
import { NavigationContainer } from '@react-navigation/native';
import SplashScreen from 'react-native-splash-screen'
import NetInfo from "@react-native-community/netinfo";
import NetworkError from './Src/Network/NetworkError';
import {Provider} from 'react-redux'
import reduxStore from './Src/Utils/configReduxStore'

const store = reduxStore()

const App = () => {
  const [status, setStatus] = useState(true);

  useEffect(()=>{
    SplashScreen.hide();
    NetInfo.addEventListener(state => {
      // this.setState({ netStatus: state.isConnected })
      setStatus(state.isConnected)
    });
  },[])

  return (
    <Provider store={store}>
      <NavigationContainer>
        {!status ? <NetworkError /> : null}
        <Navigator/>
      </NavigationContainer>
    </Provider>
  )
}

export default App;